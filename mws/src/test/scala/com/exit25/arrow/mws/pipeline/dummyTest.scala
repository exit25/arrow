package com.exit25.arrow.mws.pipeline

import java.io.File

import com.exit25.arrow.mws.{AWSLowestOfferListing, AWSCompetitivePrice}
import com.exit25.arrow.mws.auth.AWSSellerCredentials
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfter, ShouldMatchers, GivenWhenThen, FunSpec}
import com.exit25.arrow.mws.pipeline.sources.{GetLowestOfferListingsForASIN, GetCompetitivePricingForASIN, ListMatchingProducts}

/**
 * Created by masinoa on 9/22/15.
 */
class dummyTest extends FunSpec with GivenWhenThen with ShouldMatchers with BeforeAndAfter{

  describe("a dummy request"){
    it("should make a request"){
      Given("the right parameters")
      val configParams = ConfigFactory.parseFile(new File("./conf/application.conf"))
      val credentials_root_path = "arrow.mws.credentials"
      val accessKeyID = configParams.getString(s"$credentials_root_path.accessKeyID")
      val accessSecretKey = configParams.getString(s"$credentials_root_path.accessSecretKey")
      val sellerID = configParams.getString(s"$credentials_root_path.sellerID")
      val marketPlaceID = configParams.getString("arrow.mws.marketplaceID")
      val creds = AWSSellerCredentials(accessKeyID, accessSecretKey, sellerID)

      //list matching products based on keyword search
      //val lmp = ListMatchingProducts(marketPlaceID,"Calculus Stewart", creds, "Books")
      val lmp = ListMatchingProducts(marketPlaceID,"The PowerScore LSAT Logic Games Bible", creds, "Books")
      val products = lmp.products()

      //update products to get competitive pricing
      val gcp4asin = GetCompetitivePricingForASIN(marketPlaceID,creds)
      gcp4asin.stageResult(products)

      //update products to get lowest offer listing
      val glol4asin = GetLowestOfferListingsForASIN(marketPlaceID, creds)
      glol4asin.stageResult(products)

      products.foreach{item =>
        val asin: String = item.asin().getOrElse("NOASIN")
        println(s"--------------------------ITEM: $asin -----------------------------------")
        item.attributeSets().getOrElse(List.empty[AnyRef]).foreach{at =>

          println(s"Attribute: $at")
        }

        val displayRank: Int = item.displaySalesRank().getOrElse(-1)
        println(s"SALES RANK: $displayRank")

        val tiv: scala.BigDecimal = item.tradeInValue().getOrElse(scala.BigDecimal(-1.0))
        println(s"Trade In Value=${tiv.doubleValue()}")

        println("COMPETITIVE PRICING")
        item.competitivePricing().getOrElse(List.empty[AWSCompetitivePrice]).foreach{cpt =>
            val cond = cpt.condition().getOrElse("N/A")
            val id = cpt.id.getOrElse("N/A")
            val price = cpt.landedPrice().getOrElse(scala.BigDecimal(-1))
            println(s"ID: $id, CONDITION: $cond, PRICE: ${price.doubleValue()}")
        }
        println("LOWEST OFFERS")
        item.lowestOfferListings().getOrElse(List.empty[AWSLowestOfferListing]).foreach{ lol =>
          println(s"LANDED PRICE: ${lol.landedPrice().getOrElse(scala.BigDecimal(-1)).doubleValue()}")
          println(s"MULTIPLE OFFERS LOWEST PRICE: ${lol.multipleOffersAtLowestPrice().getOrElse("Unknown")}")
          val fc = lol.fulfillment().getOrElse("Unknown")
          println(s"FULLFILLMENT CHANNEL: $fc")
          val cond = lol.condition().getOrElse("Unknown")
          println(s"CONDITION: $cond")
        }
      }
    }
  }

}

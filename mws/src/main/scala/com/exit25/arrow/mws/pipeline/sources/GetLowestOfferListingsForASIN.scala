package com.exit25.arrow.mws.pipeline.sources

import com.amazonservices.mws.products.model.{ASINListType, GetLowestOfferListingsForASINRequest}
import com.exit25.arrow.mws.AWSProduct
import com.exit25.arrow.mws.auth.AWSSellerCredentials
import com.exit25.arrow.mws.client.MWSClient
import com.exit25.arrow.mws.pipeline.stages.Stage
import scala.collection.JavaConversions._

/**
 * Created by masinoa on 10/1/15.
 */
case class GetLowestOfferListingsForASIN (marketPlaceID:String,
                                     credentials:AWSSellerCredentials,
                                     asinList:Option[List[String]]=None) extends Source with Stage {


  def initializedRequest() : GetLowestOfferListingsForASINRequest = {
    val request = new GetLowestOfferListingsForASINRequest()
    request.setSellerId(credentials.sellerID)
    request.setMWSAuthToken(credentials.mwsAuthToken)
    request.setMarketplaceId(marketPlaceID)
    return request
  }

  def executeRequest(request:GetLowestOfferListingsForASINRequest) = {
    val client = MWSClient(credentials)
    client.getLowestOfferListingsForASIN(request)
  }

  def products() : List[AWSProduct] = {
    asinList match{
      case Some(al) => {
        val asinListType = new ASINListType()
        asinListType.setASIN(al)
        val request = initializedRequest()
        request.setASINList(asinListType)
        val response = executeRequest(request)
        val responseList = response.getGetLowestOfferListingsForASINResult
        (List.empty[AWSProduct]/:responseList){(l,r) => AWSProduct(r.getProduct)::l}
      }
      case _ => List.empty[AWSProduct]
    }
  }

  def stageResult(products:List[AWSProduct]) : List[AWSProduct] = {
    val asinMap: Map[String, AWSProduct] = products.map{p => (p.asin().getOrElse("NOASIN")->p)}.toMap

    val asinListType = new ASINListType()
    asinListType.setASIN(asinMap.keys.toList)
    val request = initializedRequest()
    request.setASINList(asinListType)
    val response = executeRequest(request)
    val responseList = response.getGetLowestOfferListingsForASINResult
    try{
      responseList.foreach{r =>
        val asin = r.getASIN
        asinMap.get(asin) match{
          case Some(p) => {
            try{
              p.product.setLowestOfferListings(r.getProduct.getLowestOfferListings)
            }catch{
              case e:NullPointerException => println(s"WARNING: for $asin null pointer for getlowestofferlistingsforasinresult")
            }
          }
          case _ => println(s"Key ERROR: $asin")
        }
      }
    }catch{
      case e:Exception => println(s"WARNING: Exception getlowestofferlistingsforasinresult ${e.getMessage}")
    }
    products
  }

}

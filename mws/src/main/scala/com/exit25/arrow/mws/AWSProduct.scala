package com.exit25.arrow.mws

import com.amazonservices.mws.products.model.{CompetitivePriceType, LowestOfferListingType, Product}

import scala.collection.JavaConversions._
/**
 * Created by masinoa on 10/8/15.
 */

trait NullGetter{
  def get[G](code: => G) : Option[G] = {
    try{
      Some(code)
    }catch{
      case e: NullPointerException => None
    }
  }
}

case class AWSProduct(product: Product) extends NullGetter{

  def asin() : Option[String] = {
    get{
      product.getIdentifiers.getMarketplaceASIN.getASIN
    }
  }
  
  def displaySalesRank() : Option[Int] = {
    get{
      val salesRanks = product.getSalesRankings.getSalesRank.toList.map{sr=>sr.getRank}
      if(salesRanks.isEmpty) Int.MaxValue
      else salesRanks.max
    }
  }
  
  def tradeInValue() : Option[scala.BigDecimal] = {
    get{
      scala.BigDecimal(product.getCompetitivePricing.getTradeInValue.getAmount.doubleValue())
    }
  }
  
  def competitivePricing(): Option[List[AWSCompetitivePrice]] = {
    get{
      product.getCompetitivePricing.getCompetitivePrices.getCompetitivePrice.toList.map{AWSCompetitivePrice(_)}
    }
  }

  def buyBoxNewLandedPrice(): Option[scala.BigDecimal] = {
    competitivePricing() match{
      case Some(l) =>{
        val price : Option[scala.BigDecimal] = None
        (price/:l){(p,cp) =>
          val id = cp.id().getOrElse("-1")
          if(id=="1"){
            cp.landedPrice()
          }else price
        }
      }
      case _ => None
    }
  }

  def buyBoxNewListPrice(): Option[scala.BigDecimal] = {
    competitivePricing() match{
      case Some(l) =>{
        val price : Option[scala.BigDecimal] = None
        (price/:l){(p,cp) =>
          val id = cp.id().getOrElse("-1")
          if(id=="1"){
            cp.listPrice()
          }else price
        }
      }
      case _ => None
    }
  }

  def buyBoxUsedLandedPrice(): Option[scala.BigDecimal] = {
    competitivePricing() match{
      case Some(l) =>{
        val price : Option[scala.BigDecimal] = None
        (price/:l){(p,cp) =>
          val id = cp.id().getOrElse("-1")
          if(id=="2"){
            cp.landedPrice()
          }else price
        }
      }
      case _ => None
    }
  }

  def buyBoxUsedListPrice(): Option[scala.BigDecimal] = {
    competitivePricing() match{
      case Some(l) =>{
        val price : Option[scala.BigDecimal] = None
        (price/:l){(p,cp) =>
          val id = cp.id().getOrElse("-1")
          if(id=="2"){
            cp.listPrice()
          }else price
        }
      }
      case _ => None
    }
  }

  def lowestOfferListings(): Option[List[AWSLowestOfferListing]] = {
    get{
      product.getLowestOfferListings.getLowestOfferListing.toList.map{AWSLowestOfferListing(_)}
    }
  }

  def attributeSets():Option[List[AnyRef]] = {
    get{
      product.getAttributeSets.getAny.toList
    }
  }
}

case class AWSLowestOfferListing(lol : LowestOfferListingType) extends NullGetter{

  def landedPrice(): Option[scala.BigDecimal] = {
    get{
      scala.BigDecimal(lol.getPrice.getLandedPrice.getAmount.doubleValue())
    }
  }

  def listPrice(): Option[scala.BigDecimal] = {
    get{
      scala.BigDecimal(lol.getPrice.getListingPrice.getAmount.doubleValue())
    }
  }

  def shipping(): Option[scala.BigDecimal] = {
    get{
      scala.BigDecimal(lol.getPrice.getShipping.getAmount.doubleValue())
    }
  }

  def fulfillment(): Option[String] = {
    get{
      lol.getQualifiers.getFulfillmentChannel
    }
  }

  def condition(): Option[String] = {
    get{
      lol.getQualifiers.getItemCondition
    }
  }

  def subCondition(): Option[String] = {
    get{
      lol.getQualifiers.getItemSubcondition
    }
  }

  def sellerFeedback(): Option[String] = {
    get{
      lol.getQualifiers.getSellerPositiveFeedbackRating
    }
  }

  def multipleOffersAtLowestPrice(): Option[String] = {
    get{
      lol.getMultipleOffersAtLowestPrice
    }
  }

}

case class AWSCompetitivePrice(cpt: CompetitivePriceType) extends NullGetter{

  def condition(): Option[String] = {
    get{
      cpt.getCondition
    }
  }

  def subCondition() = {
    get{
      cpt.getSubcondition
    }
  }

  def id(): Option[String] = {
    get{
      cpt.getCompetitivePriceId
    }
  }

  def landedPrice(): Option[scala.BigDecimal] = {
    get{
      scala.BigDecimal(cpt.getPrice.getLandedPrice.getAmount.doubleValue())
    }
  }

  def listPrice(): Option[scala.BigDecimal] = {
    get{
      scala.BigDecimal(cpt.getPrice.getListingPrice.getAmount.doubleValue())
    }
  }

  def shipping(): Option[scala.BigDecimal] = {
    get{
      scala.BigDecimal(cpt.getPrice.getShipping.getAmount.doubleValue())
    }
  }


}



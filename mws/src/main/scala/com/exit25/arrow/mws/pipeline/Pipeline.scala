package com.exit25.arrow.mws.pipeline

import com.exit25.arrow.mws.pipeline.sources.Source

/**
 * Created by masinoa on 9/29/15.
 */
case class Pipeline(sources:List[Source], pipes:List[Pipe]) {

  def execute() : Unit = {
    sources.foreach{source =>
      val products =source.products()
      pipes.foreach{pipe =>
        pipe.execute(products)
      }
    }
  }

}

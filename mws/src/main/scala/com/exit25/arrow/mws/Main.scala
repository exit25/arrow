package com.exit25.arrow.mws

import java.io.File

import com.exit25.arrow.mws.auth.AWSSellerCredentials
import com.exit25.arrow.mws.pipeline.sinks.CsvSink
import com.exit25.arrow.mws.pipeline.sources.{GetCompetitivePricingForASIN, GetLowestOfferListingsForASIN, ListMatchingProducts}
import com.exit25.arrow.mws.pipeline.stages.{FBADifferentialFilter, SalesRankFilter}
import com.exit25.arrow.mws.pipeline.{Pipe, Pipeline}
import com.typesafe.config.ConfigFactory

/**
 * Created by masinoa on 10/10/15.
 */
object Main {

  def main(args: Array[String]): Unit = {

    val queries = List("nuclear physics",
                       "analytical chemistry",
                       "organic chemistry",
                       "calculus",
                       "Criminal Psychology")/*,
                        "Linear Algebra",
                        "Introduction to Microbiology",
                        "Health Services","" +
                        "Criminal Justice",
                        "Law for Paralegals",
                        "Forensic Science",
                        "Introduction to Chemistry",
                        "Philosophy",
                        "Differential Equations",
                        "Materials Science",
                        "Evangelical Theology").map(_.toLowerCase)*/

    val configParams = ConfigFactory.parseFile(new File("./mws/conf/applications.conf"))
    val credentials_root_path = "mws.aws.credentials"
    val accessKeyID = configParams.getString(s"$credentials_root_path.accessKeyID")
    val accessSecretKey = configParams.getString(s"$credentials_root_path.accessSecretKey")
    val sellerID = configParams.getString(s"$credentials_root_path.sellerID")
    val marketPlaceID = configParams.getString("mws.aws.marketplaceID")
    val asinBaseUrl = configParams.getString("mws.aws.baseAsinUrl")
    val creds = AWSSellerCredentials(accessKeyID, accessSecretKey, sellerID)

    val outputFile = "/Users/masinoa/Desktop/awsTest.csv"

    CsvSink(outputFile,asinBaseUrl).writeHeaders()

    queries.foreach(query =>{
      println(s"------------------------Query: $query -------------------------")
      val source = ListMatchingProducts(marketPlaceID, query, creds, "Books")
      println(s"Found ${source.products().length} products")
      val stages = List(GetCompetitivePricingForASIN(marketPlaceID, creds),
                        GetLowestOfferListingsForASIN(marketPlaceID,creds),
                        SalesRankFilter(500000),
                        FBADifferentialFilter(BigDecimal(1.00))
                        )
      val printHeaders = false
      val sink = CsvSink(outputFile,asinBaseUrl,printHeaders)
      val pipe = Pipe(stages, sink)
      val pipeline = Pipeline(List(source),List(pipe))
      pipeline.execute()

    })

  }

}

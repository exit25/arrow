package com.exit25.arrow.mws.auth

import com.typesafe.config.Config

case class AWSSellerCredentials (accessKeyID:String, accessSecretKey:String, sellerID: String, mwsAuthToken:String = "NONE")

object AWSSellerCredentials{

  def apply(conf:Config) : AWSSellerCredentials= {
    val akid = conf.getString("accessKeyID")
    val ask = conf.getString("accessSecretKey")
    val sid = conf.getString("sellerID")
    apply(akid, ask, sid)
  }

}
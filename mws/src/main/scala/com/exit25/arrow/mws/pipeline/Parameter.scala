package com.exit25.arrow.mws.pipeline

/**
 * Created by masinoa on 9/21/15.
 */
class Parameter(name: String, value:Any) {}

class StringParameter(name:String, value:String) extends Parameter(name,value)
class IntParameter(name:String, value:Int) extends Parameter(name,value)
class IntRangeParameter(name:String, value:(Int,Int)) extends Parameter(name,value)

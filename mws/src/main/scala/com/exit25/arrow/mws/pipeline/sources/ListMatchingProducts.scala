package com.exit25.arrow.mws.pipeline.sources

import com.amazonservices.mws.products.MarketplaceWebServiceProductsException
import com.amazonservices.mws.products.model.{AttributeSetList, ListMatchingProductsRequest}
import com.exit25.arrow.mws.AWSProduct
import com.exit25.arrow.mws.auth.AWSSellerCredentials
import com.exit25.arrow.mws.client.MWSClient
import scala.collection.JavaConversions._
import scala.xml.{Node, Elem}


/**
 * Created by masinoa on 9/22/15.
 * Executes the ListProducts API request to the MWS API
 */

case class ListMatchingProducts(marketPlaceID:String,
                           query:String,
                           credentials:AWSSellerCredentials,
                           queryContextId:String = "All") extends Source{

  /**
   * @return
   * executes the List Product request to the Amazon MWS API
   *
   */
  def products() : List[AWSProduct] = {
    val client = MWSClient(credentials)
    // Create a request.
    val request = new ListMatchingProductsRequest()
    request.setSellerId(credentials.sellerID)
    request.setMWSAuthToken(credentials.mwsAuthToken)
    request.setMarketplaceId(marketPlaceID)
    request.setQuery(query)
    request.setQueryContextId(queryContextId)
    try{
      val response = client.listMatchingProducts(request)


      val xml: Elem = scala.xml.XML.loadString(response.toXML)
      val authorTitle = asinTitelAuthorMap(xml) match{
        case Some(m) => m
        case _ => Map.empty[String,(String,String)]
      }

      val result = response.getListMatchingProductsResult
      val products = result.getProducts.getProduct.toList

      products.foreach{p =>
        val asin = p.getIdentifiers.getMarketplaceASIN.getASIN
        val (t,a) = authorTitle.getOrElse(asin, ("NONE","NONE"))
        p.setAttributeSets(new AttributeSetList(List("Author"->a, "Title"->t)))
      }
      products.map{AWSProduct(_)}

    }catch {
      case ex: MarketplaceWebServiceProductsException => {
        val rhmd = ex.getResponseHeaderMetadata();
        if(rhmd != null) {
          println("RequestId: "+rhmd.getRequestId());
          println("Timestamp: "+rhmd.getTimestamp());
        }
        println("Message: "+ex.getMessage());
        println("StatusCode: "+ex.getStatusCode());
        println("ErrorCode: "+ex.getErrorCode());
        println("ErrorType: "+ex.getErrorType());
        println("ErrorType: "+ex.getErrorType());
        List.empty[AWSProduct]
      }
    }
  }

  private def findNode(label:String, root:Either[Elem,Node]) : Option[Node] = {
    val start : Option[Node] = None
    root match{
      case Left(elem) => {
        (start/:elem.child){(o ,c) =>
          if(c.label==label)Some(c)
          else o
        }
      }
      case Right(node) =>{
        (start/:node.child){(o,c) =>
          if(c.label==label)Some(c)
          else o
        }
      }
    }
  }

  private def listMatchingProductsResultElem(xml:Elem): Option[Node] = {
    val start : Option[Node] = None
    (start/:xml.child){(o,c) =>
      if(c.label=="ListMatchingProductsResult")Some(c)
      else o
    }
  }

  private def productsElem(rootXml:Elem) : Option[Node] = {
    findNode("ListMatchingProductsResult", Left(rootXml)) match{
      case Some(n) => findNode("Products", Right(n))
      case _ => None
    }
  }

  private def asinFromProductNode(productNode: Node) :Option[String] = {
    findNode("Identifiers", Right(productNode)) match{
      case Some(ids) =>{
        findNode("MarketplaceASIN", Right(ids)) match{
          case Some(mpa) =>{
            findNode("ASIN", Right(mpa)) match{
              case Some(asinNode) => {Some(asinNode.text)}
              case _ => None
            }
          }
          case _ => None
        }
      }
      case _ => None
    }
  }

  private def attributesFromProductNode(productNode: Node, attributeLabels: List[String]) : Option[Map[String,String]] = {
    findNode("AttributeSets", Right(productNode)) match{
      case Some(as) => {
        findNode("ItemAttributes", Right(as)) match{
          case Some(ns2) => {
            val m: Map[String, String] = (Map.empty[String,String]/:attributeLabels){(tm,a) =>
              findNode(a,Right(ns2)) match{
                case Some(an) => tm.+(a->an.text)
                case _ => tm
              }
            }
            Some(m)
          }
          case _ => None
        }
      }
      case _ => None
    }
  }

  private def titleAuthorFromProductNode(productNode: Node) = attributesFromProductNode(productNode, List("Author", "Title"))

  private def asinTitelAuthorMap(rootXML: Elem) : Option[Map[String,(String,String)]]={
    productsElem(rootXML) match{
      case Some(pe) => {
        Some((Map.empty[String,(String,String)]/:pe.child){(tm,pn)=>
          asinFromProductNode(pn) match{
            case Some(asin) => {
              titleAuthorFromProductNode(pn) match{
                case Some(m) =>{
                  val author = m.getOrElse("Author","NOAUTHOR")
                  val title = m.getOrElse("Title","NOTITLE")
                  tm.+(asin->(title,author))
                }
                case _ => tm
              }
            }
            case _ => tm
          }
        })
      }
      case _ => None
    }
  }


}

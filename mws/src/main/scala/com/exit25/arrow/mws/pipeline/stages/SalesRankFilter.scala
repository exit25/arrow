package com.exit25.arrow.mws.pipeline.stages

import com.exit25.arrow.mws.AWSProduct


/**
 * Created by masinoa on 10/8/15.
 */
case class SalesRankFilter(threshold:Int) extends Stage{

  def stageResult(products:List[AWSProduct]) : List[AWSProduct] = {
    (List.empty[AWSProduct]/:products){(l,p) =>
      p.displaySalesRank() match{
        case Some(sr) => {
          if(sr<threshold) p::l
          else l
        }
        case _ => l
      }
    }
  }

}

package com.exit25.arrow.mws.pipeline.sinks

import java.io._

import com.exit25.arrow.mws.AWSProduct

/**
 * Created by masinoa on 10/9/15.
 */
case class CsvSink(filePath:String, baseASINUrl : String, printHeaders:Boolean = true, overwrite: Boolean = false) extends Sink {

  val NP = "NOT_PROVIDED"
  val used = 'Used
  val merchant = 'Merchant
  val amazon = 'Amazon

  def url_from(asin:String) = {
    s"$baseASINUrl/$asin"
  }

  val headers = List("ASIN",
    "URL",
    "SALES_RANK",
    "TRADE_IN_VALUE",
    "BUY_BOX_NEW",
    "BUY_BOX_USED",
    "FBA_LOWEST_LANDED_PRICE:CONDITION",
    "MERCHANT_LOWEST_LANDED_PRICE:CONDITION")

  def writeHeaders() = {
    val printer = if(overwrite) printToFile(filePath) _ else appendToFile(filePath) _
    printer { pw => pw.println((""/:headers){(l,h)=>s"$l$h,"}.dropRight(1))}
  }

  def processResult(products: List[AWSProduct]) : Unit = {

    val printer = if(overwrite) printToFile(filePath) _ else appendToFile(filePath) _

    printer{ pw =>
      if(printHeaders)pw.println((""/:headers){(l,h)=>s"$l$h,"}.dropRight(1))
      products.foreach{p=>
        val asin = p.asin().getOrElse(NP)
        val url = url_from(asin)
        val sr = p.displaySalesRank().getOrElse(BigDecimal(-1))
        val tiv = p.tradeInValue().getOrElse(BigDecimal(-1))
        val bbn = p.buyBoxNewLandedPrice().getOrElse(BigDecimal(-1))
        val bbu = p.buyBoxUsedLandedPrice().getOrElse(BigDecimal(-1))
        val (fbalp, fbaloc) = p.lowestOfferListings() match {
          case Some(lol) => {
            ((BigDecimal(-1),"")/:lol){(t,no)=>
              no.fulfillment() match{
                case Some(f) =>{
                  if(Symbol(f)==amazon){
                    val cond = no.condition().getOrElse("Unknown")
                    val price = no.landedPrice().getOrElse(BigDecimal(-1))
                    val clp = t._1
                    if(clp == -1 || price<clp)(price, cond)
                    else t
                  }
                  else t
                }
                case _ => t
              }
            }
          }
          case _ => (BigDecimal(-1),"N/A")
        } //end fbalo, fbaloc
      val (mlp, mloc) = p.lowestOfferListings() match {
          case Some(lol) => {
            ((BigDecimal(-1),"")/:lol){(t,no)=>
              no.fulfillment() match{
                case Some(f) =>{
                  if(Symbol(f)==merchant){
                    val cond = no.condition().getOrElse("Unknown")
                    val price = no.landedPrice().getOrElse(BigDecimal(-1))
                    val clp = t._1
                    if(clp == -1 || price<clp)(price, cond)
                    else t
                  }
                  else t
                }
                case _ => t
              }
            }
          }
          case _ => (BigDecimal(-1),"N/A")
        } //end mlp,mloc
        pw.println(s"$asin,$url,$sr,$tiv,$bbn,$bbu,$fbalp:$fbaloc,$mlp:$mloc")
      }
    }
  }

  def printToFile(path: String)(op: PrintWriter => Unit) {
    val f = new File(path)
    if (f.exists) f.delete
    if (!f.getParentFile.exists())f.getParentFile.mkdirs()
    val pw = new PrintWriter(new File(path))
    try {
      op(pw)
    } finally {
      pw.close()
    }
  }

  def appendToFile(path: String)(op: PrintWriter => Unit) {
    val f = new File(path)
    if (!f.exists()){
      if(!f.getParentFile.exists())f.getParentFile.mkdirs()
      f.createNewFile()
    }
    val pw = new PrintWriter(new BufferedWriter(new FileWriter(path, true)))
    try { {
      op(pw)
    }
    } finally {
      pw.close()
    }
  }

}

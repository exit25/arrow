package com.exit25.arrow.mws.pipeline.sources

import com.exit25.arrow.mws.AWSProduct

/**
 * Created by masinoa on 9/26/15.
 */
trait Source {
  def products() : List[AWSProduct]
}

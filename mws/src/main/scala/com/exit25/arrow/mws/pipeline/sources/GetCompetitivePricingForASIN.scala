package com.exit25.arrow.mws.pipeline.sources

import com.exit25.arrow.mws.AWSProduct
import com.exit25.arrow.mws.auth.AWSSellerCredentials
import com.exit25.arrow.mws.client.MWSClient
import com.amazonservices.mws.products.model.{ASINListType, GetCompetitivePricingForASINRequest, Product}
import com.exit25.arrow.mws.pipeline.stages.Stage
import scala.collection.JavaConversions._

/**
 * Created by masinoa on 9/27/15.
 */
case class GetCompetitivePricingForASIN(marketPlaceID:String,
                                        credentials:AWSSellerCredentials,
                                        asinList:Option[List[String]]=None) extends Source with Stage{

  private def initializedRequest() : GetCompetitivePricingForASINRequest = {
    val request = new GetCompetitivePricingForASINRequest()
    request.setSellerId(credentials.sellerID)
    request.setMWSAuthToken(credentials.mwsAuthToken)
    request.setMarketplaceId(marketPlaceID)
    return request
  }

  def executeRequest(request:GetCompetitivePricingForASINRequest) = {
    val client = MWSClient(credentials)
    client.getCompetitivePricingForASIN(request)
  }

  def products() : List[AWSProduct] = {
    asinList match{
      case Some(al) => {
        val asinListType = new ASINListType()
        asinListType.setASIN(al)
        val request = initializedRequest()
        request.setASINList(asinListType)
        val response = executeRequest(request)
        val responseList = response.getGetCompetitivePricingForASINResult
        (List.empty[Product]/:responseList){(l,r) => r.getProduct::l}.map(AWSProduct(_))
      }
      case _ => List.empty[AWSProduct]
    }
  }

  def stageResult(products:List[AWSProduct]) : List[AWSProduct] = {
    val asinMap: Map[String, AWSProduct] = products.map{p => (p.asin().getOrElse("NOASIN")->p)}.toMap


    val asinListType = new ASINListType()
    asinListType.setASIN(asinMap.keys.toList)
    val request = initializedRequest()
    request.setASINList(asinListType)
    val response = executeRequest(request)
    val responseList = response.getGetCompetitivePricingForASINResult
    try{
      responseList.foreach{r =>
        val asin = r.getASIN
        asinMap.get(asin) match{
          case Some(p) => {
            p.product.setCompetitivePricing(r.getProduct.getCompetitivePricing)
          }
          case _ => println(s"Key ERROR: $asin")
        }
      }
    }
    catch{
      case e:NullPointerException => println("WARNING: null pointer in getCompetivePricingForASINResult")
    }
    products
  }

}

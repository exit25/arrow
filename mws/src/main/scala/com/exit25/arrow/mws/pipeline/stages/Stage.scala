package com.exit25.arrow.mws.pipeline.stages

import com.exit25.arrow.mws.AWSProduct

/**
 * Created by masinoa on 9/21/15.
 */
trait Stage {

  def stageResult(products: List[AWSProduct]) : List[AWSProduct]

}


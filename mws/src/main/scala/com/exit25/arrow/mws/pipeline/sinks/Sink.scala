package com.exit25.arrow.mws.pipeline.sinks

import com.exit25.arrow.mws.AWSProduct

/**
 * Created by masinoa on 9/26/15.
 */
trait Sink {

  def processResult(products: List[AWSProduct]) : Unit

}

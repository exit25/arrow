package com.exit25.arrow.mws.client

import java.io.File

import com.amazonservices.mws.products.{MarketplaceWebServiceProductsAsyncClient, MWSEndpoint, MarketplaceWebServiceProductsConfig}
import com.exit25.arrow.mws.auth.AWSSellerCredentials
import com.typesafe.config.{Config, ConfigFactory}

/**
 * Created by masinoa on 9/24/15.
 */
object MWSClient {

  def apply(credentials: AWSSellerCredentials) : MarketplaceWebServiceProductsAsyncClient = {
    val mwsconfig = new MarketplaceWebServiceProductsConfig()
    val serviceUrl = "https://mws.amazonservices.com"
    mwsconfig.setServiceURL(serviceUrl)
    //val appName = configParams.getString("mws.app_name")
    //val appVersion = configParams.getString("mws.version")
    val client = new MarketplaceWebServiceProductsAsyncClient(credentials.accessKeyID,
      credentials.accessSecretKey,"MWSClient","1.0",mwsconfig, null)
    return client
  }

}

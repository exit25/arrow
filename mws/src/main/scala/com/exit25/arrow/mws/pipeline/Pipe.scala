package com.exit25.arrow.mws.pipeline

import com.exit25.arrow.mws.AWSProduct
import com.exit25.arrow.mws.pipeline.sinks.Sink
import com.exit25.arrow.mws.pipeline.stages.Stage

/**
 * Created by masinoa on 9/26/15.
 */
case class Pipe(stages:List[Stage], sink:Sink) {


  def execute(sourceProducts: List[AWSProduct]) : Unit = {

    val resultProducts = (sourceProducts/:stages){(products,stage) =>
      stage.stageResult(products)
    }
    sink.processResult(resultProducts)

  }

}

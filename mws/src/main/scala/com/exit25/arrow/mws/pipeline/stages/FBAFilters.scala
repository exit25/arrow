package com.exit25.arrow.mws.pipeline.stages

import com.exit25.arrow.mws.{AWSLowestOfferListing, AWSProduct}


/**
 * Created by masinoa on 10/8/15.
 */

trait FBAFilter {
  val MERCHANT = 'merchant
  val AMAZON = 'amazon
}

object FBAOnlyFilter extends Stage with FBAFilter{

  def stageResult(products:List[AWSProduct]) : List[AWSProduct] = {
    (List.empty[AWSProduct]/:products){(l,p) =>
      p.lowestOfferListings()match{
        case Some(lol) =>{
          val hasFBA = (false/:lol){(b,lo) =>
            lo.fulfillment() match{
              case Some(s) => b || Symbol(s.toLowerCase)==AMAZON
              case _ => b
            }
          }
          if(hasFBA) l
          else p::l
        }
        case _ => l
      }
    }
  }
}

case class FBADifferentialFilter(differential: BigDecimal) extends Stage with FBAFilter {

  def stageResult(products: List[AWSProduct]) : List[AWSProduct] = {
    (List.empty[AWSProduct]/:products){(l,p) =>
      p.lowestOfferListings() match{

        case Some(lol) =>{

          val (fbaOffers,merchantOffers) = ((List.empty[AWSLowestOfferListing],List.empty[AWSLowestOfferListing])/:lol){(t,o)=>
            val fbao = t._1
            val mo = t._2
            val isFBAOffer = o.fulfillment()match{
              case Some(s) => Symbol(s.toLowerCase)==AMAZON
              case _ => false
            }
            if(isFBAOffer)(o::fbao,mo)
            else(fbao,o::mo)
          }

          if(fbaOffers.isEmpty) p::l
          else if(merchantOffers.isEmpty) l
          else{

            val minFBAO = (BigDecimal(-1)/:fbaOffers){(lo,no) =>
              no.landedPrice() match{
                case Some(v) => {
                  if(lo.doubleValue() == -1) v
                  else if(lo.compare(v) == 1) v
                  else lo
                }
                case _ => lo
              }
            }

            val minMerchant = (BigDecimal(-1)/:merchantOffers){(lo,no) =>
              no.landedPrice() match{
                case Some(v) => {
                  if(lo.doubleValue() == -1) v
                  else if(lo.compare(v) == 1) v
                  else lo
                }
                case _ => lo
              }
            }

            if(differential.compare(minFBAO-minMerchant) == -1) p::l else l
          }
        }

        case _ => l
      }
    }
  }

}

AWS product arbitrage analysis library. Provides functionality to allow users to search AWS product inventory
with various criteria and process results to identify candidate items for purchase and resale. 

REQUIRES:
Amazon MWS API Java client library
Amazon MWS API keys

Development Notes:
Analysis can proceed in stages. Every analysis is expected to be constructed from a source, stages, and a sink. The
source generates the initial list of items to consider. The stages analyze an item based on criteria. The sink
collects items that make it through the pipe of stages. The stages can come in the following types:

1. Generating a list of candidate items from MWS based on user parameters such as keywords, contexts (e.g. books)
2. Filter items based on item properties - e.g. salesrank below 1000
2. Getting offer information for the items. This can be competitive offer information or lowest offer information.
3. Filtering items based individual offers - e.g. good condition offers with landing price less than trade in value
4. Filter items based on offer set parameters - e.g. require all offers to be non FBA

Stages can be chained together to identify items and offers to consider.

The examples below are meant to illustrate how stages can be chained together. The items preceded with an * require
calls to the Amazon MWS API. As the examples illustrate, it is best practice to filter items based on item attributes
given in the initial search prior to executing stages require further MWS calls in order to reduce API calls

Example 1: Find all items matching the keywords, "my keywords", that have a trade in value greater than the landing price
and for which the offered item is in good condition or better:
*Source 1: ListMatchingProducts with keywords
Source Filter  2: Filter non trade in eligible
*Stage 3: GetCompetivePricingForASIN (contains trade in value) - can be done for upto 10 products
    #LOOP: repeat for each item
    *Stage 4: GetLowestOfferListingsForASIN
    Stage  5: Filter offers with landing price greater than trade in value
    Stage  6: Filter offers less than good condition

Example 2: Find all items and the N lowest offers matching the keywords, "my keywords", with sales rank above SR,
for which there are no FBA offers, there is at least non-FBA offer with a landing price X that is at least Y dollars
less than the used buy box price (should take the lowest if there are more than one) and for which the offered item
is in at least good condition.
*Source 1: ListMatchingProducts with keywords
Source Filter  2: Filter those with sales rank below SR (sales rank provided in step 1)
*Stage 3: GetLowestOfferListingsForASIN (contains lowest offers by condition and fulfilment channel) (can be done for up to 10 proudcts)
*Stage 3: GetCompetivePricingForASIN (contains buy box price) (can be done for up to 10 products)
    #LOOP repeat for each item
    Stage  5: Break if there is an FBA offer in lowest
    Stage  6: Filter offers less than good condition
    Stage  7: Find lowest N offers and return


package com.exit25.arrow.neo4j.test

import com.exit25.arrow.neo4j.model.Neo4jBrowseNode
import com.exit25.arrow.neo4j.{BrowseNodeQuerier, ConnectionFactory}
import org.anormcypher.Cypher
import org.scalatest.{ShouldMatchers, FlatSpec}

import scala.concurrent.ExecutionContextExecutor

/**
 * Created by masinoa on 11/8/15.
 */
class BrowseNodeQueryTests extends FlatSpec with ShouldMatchers{

  implicit val con = ConnectionFactory.apply("./conf/application.conf")
  implicit val ec: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  val bnq = BrowseNodeQuerier(con,ec)


  "The deleteAllNodes method" should "result in an empty stream" in{
    bnq.deleteAllNodes()
    val cmd = "MATCH (n) RETURN n"
    val req = Cypher(cmd)
    val stream = req()
    stream.isEmpty should equal(true)
  }

  val now = Neo4jBrowseNode.today()
  val books = Neo4jBrowseNode("1000", "Books", now, false)
  val textBooks = Neo4jBrowseNode("1100", "Text Books", now, false)
  val mathBooks = Neo4jBrowseNode("1110", "Math Books", now, false)
  val physicsBooks = Neo4jBrowseNode("1120", "Physics Books", now, false)
  val electronics = Neo4jBrowseNode("2000", "Electronics", now, false)

  val algebraBooks = Neo4jBrowseNode("1111", "Algebra Books", now, false)
  "The createBrowseNode method" should "result in node creation" in{
    bnq.createBrowseNode(books) should equal(true)
    bnq.createBrowseNode(textBooks) should equal(true)
    bnq.createBrowseNode(electronics) should equal(true)
    List(mathBooks,physicsBooks, algebraBooks).foreach{bn =>
      bnq.createBrowseNode(bn) should equal(true)
    }
  }

  "The createParentsRelationship method" should "result in relation creation" in{
    bnq.createParentRelationships(textBooks,List(books)) should equal(true)
    bnq.createParentRelationships(mathBooks, List(textBooks)) should equal(true)
    bnq.createParentRelationships(physicsBooks, List(textBooks)) should equal(true)
    bnq.createParentRelationships(algebraBooks, List(mathBooks)) should equal(true)
  }

  "The BrowseNodQuery.findBrowseNode method" should "find the Books node" in {
    val books: Option[Neo4jBrowseNode] = bnq.findNode("1000")
    books match{
      case Some(b) => {
        b.id should equal("1000")
        b.name should equal("Books")
        b.growthCompleted should equal(false)
        b.grownOn.getTime should equal(now.getTime)
      }
      case _ => fail("Did dot return Books node")
    }
  }

  "The BrowseNodeQuery.findChildren method" should "find the math and physics for the  books nodes" in {
    bnq.findChildren(textBooks) match{
      case Some(children) =>{
        children.length should equal(2)
        val names = children.map{_.name}
        names.contains(mathBooks.name) should equal(true)
        names.contains(physicsBooks.name) should equal(true)
      }
      case _ => fail("No children found")
    }
  }

  "The BrowseNodeQuery.findLeafNodes method" should "find the algebra and physics for the books and textbooks node " in{
    List(books, textBooks).foreach{n =>
      bnq.findLeafNodes(n) match{
        case Some(leaves) =>{
          leaves.length should equal(2)
          val names = leaves.map{_.name}
          names.contains(algebraBooks.name) should equal(true)
          names.contains(physicsBooks.name) should equal(true)
        }
        case _ => fail("No leaves found")
      }
    }
  }


  "The BrowseNodeQuery.updateNode method" should "return the updated physics node" in{
    bnq.updateBrowseNode(Neo4jBrowseNode(physicsBooks.id, physicsBooks.name+"blah", physicsBooks.grownOn, physicsBooks.growthCompleted)) match{
      case Some(n) => {
        n.id should equal(physicsBooks.id)
        n.name should equal(physicsBooks.name+"blah")
        n.grownOnString should equal(physicsBooks.grownOnString)
        n.growthCompleted should equal(physicsBooks.growthCompleted)
      }
      case _ => fail("updated node failed")
    }
  }

}

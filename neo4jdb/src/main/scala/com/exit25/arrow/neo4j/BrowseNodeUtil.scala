package com.exit25.arrow.neo4j

import java.sql.Date
import java.text.SimpleDateFormat

import com.exit25.arrow.neo4j.model.Neo4jBrowseNode
import org.anormcypher.NeoNode

/**
 * Created by masinoa on 11/21/15.
 */
object BrowseNodeUtil{

  val df = new SimpleDateFormat("yyyy-MM-dd")

  def NeoNode2BrowseNode(n: NeoNode) = {
    val props = n.props
    val name = props("name").toString
    val fid = props("id").toString
    val grownOn = new Date(df.parse(props("grownOn").toString).getTime())
    val gc = props("growthCompleted").toString.toLowerCase.toBoolean
    Neo4jBrowseNode(fid, name, grownOn, gc)
  }

}

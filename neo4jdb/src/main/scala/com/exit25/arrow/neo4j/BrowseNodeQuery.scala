package com.exit25.arrow.neo4j

import java.text.SimpleDateFormat
import com.exit25.arrow.neo4j.model.Neo4jBrowseNode
import org.anormcypher.{NeoNode, CypherResultRow, Cypher, Neo4jREST}

import scala.concurrent.ExecutionContextExecutor

/**
 * Created by masinoa on 11/7/15.
 */

case class BrowseNodeQuerier(connection:Neo4jREST, context:ExecutionContextExecutor) {

  implicit val ec: ExecutionContextExecutor = context
  implicit val con = connection
  val df = new SimpleDateFormat("yyyy-MM-dd")

  def deleteAllNodes() = {
    val cmd = "MATCH (n) DETACH DELETE n"
    Cypher(cmd).execute()
  }

  def createBrowseNode(n:Neo4jBrowseNode) = {
    val cmd = s"""CREATE (:BROWSE_NODE {id:${n.id}, name:"${n.name}", grownOn:"${n.grownOnString}", growthCompleted:${n.growthCompleted} })"""
    Cypher(cmd).execute()
  }

  def updateBrowseNode(n: Neo4jBrowseNode) = {

    val us = s"""SET n.name="${n.name}" SET n.growthCompleted=${n.growthCompleted} SET n.grownOn="${n.grownOnString}" SET n.id=${n.id} """
    val cmd = s"MATCH (n:BROWSE_NODE {id:${n.id}}) $us RETURN n"
    val req = Cypher(cmd)
    val stream = req()
    try{
      val rslt = stream.map{ (row: CypherResultRow) =>
        BrowseNodeUtil.NeoNode2BrowseNode(row[NeoNode]("n"))
      }.toList
      if(rslt.length==1 && rslt.head.id==n.id)Some(rslt.head)
      else None
    } catch {
      case e:Exception => None
    }
  }

  def createParentRelationships(child:Neo4jBrowseNode, parents: List[Neo4jBrowseNode], unique:Boolean = false) = {
    (true/:parents){(br,p) =>
      val pcmd = if(unique) s"""MATCH (p:BROWSE_NODE {id:${p.id}}), (c:BROWSE_NODE {id:${child.id}}) CREATE UNIQUE (c)-[:CHILD_OF]->(p)"""
                 else s"""MATCH (p:BROWSE_NODE {id:${p.id}}), (c:BROWSE_NODE {id:${child.id}}) CREATE (c)-[:CHILD_OF]->(p)"""
      br && Cypher(pcmd).execute()
    }
  }

  def createChildRelationships(parent:Neo4jBrowseNode, children: List[Neo4jBrowseNode], unique:Boolean = false) = {
    (true/:children){(br,c) =>
      val pcmd = if(unique)s"""MATCH (p:BROWSE_NODE {id:${parent.id}}), (c:BROWSE_NODE {id:${c.id}}) CREATE UNIQUE (c)-[:CHILD_OF]->(p)"""
                 else s"""MATCH (p:BROWSE_NODE {id:${parent.id}}), (c:BROWSE_NODE {id:${c.id}}) CREATE (c)-[:CHILD_OF]->(p)"""
      br && Cypher(pcmd).execute()
    }
  }

  def findNode(id:String)={
    val cmd = s"MATCH (n:BROWSE_NODE {id:$id}) return n"
    val req = Cypher(cmd)
    val stream = req()

    try{
      val rslt = stream.map{ (row: CypherResultRow) =>
       BrowseNodeUtil.NeoNode2BrowseNode(row[NeoNode]("n"))
      }.toList
      if(rslt.length==1 && rslt.head.id==id)Some(rslt.head)
      else None
    } catch {
      case e:Exception => None
    }
  }

  def findDescendants(bn:Neo4jBrowseNode): Option[List[Neo4jBrowseNode]]= {
    val cmd = s"MATCH (:BROWSE_NODE {id:${bn.id}})<-[:CHILD_OF*]-(child:BROWSE_NODE) RETURN child"
    val req = Cypher(cmd)
    val stream = req()
    try{
      Some(
        stream.map{ row =>
          BrowseNodeUtil.NeoNode2BrowseNode(row[NeoNode]("child"))
        }.toList
      )
    }catch{
      case e:Exception => None
    }
  }

  def findChildren(bn:Neo4jBrowseNode): Option[List[Neo4jBrowseNode]] = {
    val cmd = s"MATCH (:BROWSE_NODE {id:${bn.id}})<-[:CHILD_OF]-(child:BROWSE_NODE) RETURN child"
    val req = Cypher(cmd)
    val stream = req()
    try{
      Some(
        stream.map{ row =>
          BrowseNodeUtil.NeoNode2BrowseNode(row[NeoNode]("child"))
        }.toList
      )
    }catch{
      case e:Exception => None
    }
  }

  def findLeafNodes(bn:Neo4jBrowseNode) = {
   val cmd = s"""match (:BROWSE_NODE {id:${bn.id}})<-[:CHILD_OF*0..]-(leaf) WHERE NOT (leaf)<-[:CHILD_OF]-() RETURN leaf"""
    val req = Cypher(cmd)
    val stream = req()
    try{
      Some(
        stream.map{ row =>
          BrowseNodeUtil.NeoNode2BrowseNode(row[NeoNode]("leaf"))
        }
      )
    }catch{
      case e:Exception => None
    }
  }

}

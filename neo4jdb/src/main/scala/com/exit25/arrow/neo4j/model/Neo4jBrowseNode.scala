package com.exit25.arrow.neo4j.model

import java.sql.Date
import java.text.SimpleDateFormat

/**
 * Created by masinoa on 11/21/15.
 */
case class Neo4jBrowseNode(id: String, name:String, grownOn: Date, growthCompleted: Boolean) {

  def grownOnString = {
    val year = grownOn.toLocalDate.getYear.toString
    val month = grownOn.toLocalDate.getMonthValue
    val day = grownOn.toLocalDate.getDayOfMonth
    val monthString = if(month<10) s"0$month" else s"$month"
    val dayString = if(day<10) s"0$day" else s"$day"
    s"$year-$monthString-$dayString"
  }

}

object Neo4jBrowseNode{
  val df = new SimpleDateFormat("yyyy-MM-dd")

  def today() = {
    val now = new Date(System.currentTimeMillis()).toLocalDate
    val year = now.getYear.toString
    val month = now.getMonthValue
    val day = now.getDayOfMonth
    val monthString = if(month<10) s"0$month" else s"$month"
    val dayString = if(day<10) s"0$day" else s"$day"
    val gos = s"$year-$monthString-$dayString"
    new Date(df.parse(gos).getTime)
  }
}
package com.exit25.arrow.neo4j

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}
import org.anormcypher.Neo4jREST
import play.api.libs.ws._

/**
 * Created by masinoa on 11/7/15.
 */
object ConnectionFactory {
  
  def apply(host:String, port:Int, user:String, password:String, path:String): Neo4jREST = {
    val wsclient = ning.NingWSClient()
    Neo4jREST(host,port, path,user, password)(wsclient)
  }

  def apply(configFilePath:String, configPath:String="arrow.neo4j") : Neo4jREST = {
    val conf = ConfigFactory.parseFile(new File(configFilePath))
    val neo4jConfig = conf.getConfig(configPath)
   apply(neo4jConfig)
  }

  def apply(neo4jConfig:Config) : Neo4jREST = {
    val user = neo4jConfig.getString("user")
    val password = neo4jConfig.getString("password")
    val port = neo4jConfig.getInt("port")
    val host = neo4jConfig.getString("host")
    val path = neo4jConfig.getString("path")
    apply(host,port,user,password,path)
  }
  
}

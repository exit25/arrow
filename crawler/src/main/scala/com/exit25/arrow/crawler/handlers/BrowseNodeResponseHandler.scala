package com.exit25.arrow.crawler.handlers

import java.util.concurrent.TimeUnit

import akka.actor.{ActorSystem, Actor}
import akka.event.{Logging, LoggingAdapter}
import com.exit25.arrow.neo4j.model.Neo4jBrowseNode
import com.exit25.arrow.neo4j.{BrowseNodeQuerier, ConnectionFactory}
import com.exit25.arrow.productAd.auth.AWSKeyIdentifiers
import com.exit25.arrow.productAd.locales.Locale
import com.exit25.arrow.productAd.model.{BrowseNode}
import com.exit25.arrow.productAd.requests.BrowseNodeLookupRequest
import com.typesafe.config.Config

import scala.compat.Platform
import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration.Duration

/**
 * Created by masinoa on 11/27/15.
 */
class BrowseNodeResponseHandler(config: Config, appShutdown: ()=> Unit) extends Actor{

  lazy implicit val neo4jConf = ConnectionFactory.apply(config.getConfig("neo4j"))
  implicit val ec: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global
  lazy val neo4jQuerier = BrowseNodeQuerier(neo4jConf, ec)

  val productAdCreds = AWSKeyIdentifiers.credsFromConfig(config.getConfig("credentials"))
  val maximumRequestAttempts = config.getInt("maximumRequestAttempts")
  val system = ActorSystem("DBStorageHandlerSystem")
  val shutdownTimeoutMillis = config.getInt("shutdownTimeoutMinutes")*60*1000L

  //val log: LoggingAdapter = Logging(context.system, this)
  val log: LoggingAdapter = Logging.getLogger(context.system,this)

  var lastStorageTime : Option[Long] = None

  def receive = {

    case DeleteAllBrowseNodesMessage(time) =>{
      lastStorageTime match{
        case Some(t) => log.info("Could not delete all browsenodes due to timing conflict")
        case _ => neo4jQuerier.deleteAllNodes()
      }
    }

    case BrowseNodeLookupSuccessMessage(bn: BrowseNode) => {
      val theTime = Platform.currentTime
      lastStorageTime = Some(theTime)
      val now = Neo4jBrowseNode.today()

      val node = neo4jQuerier.findNode(bn.id) match{
        case Some(nbn) =>
        {
          val updated_node = Neo4jBrowseNode(nbn.id, nbn.name, now, true)
          neo4jQuerier.updateBrowseNode(updated_node)
          updated_node
        }
        case _ =>{
          val new_node = Neo4jBrowseNode(bn.id, bn.name, now, false)
          if(neo4jQuerier.createBrowseNode(new_node))log.info(s"CN_T,${new_node.grownOnString},${new_node.id},${new_node.name}")
          else log.info(s"CN_F,${new_node.grownOnString},${new_node.id},${new_node.name}")
          new_node
        }
      }

      val children = bn.children.getOrElse(List.empty[BrowseNode]).map{child =>
        neo4jQuerier.findNode(child.id) match{
          case Some(cn) => (cn,true)
          case _ => {
            val cn = Neo4jBrowseNode(child.id, child.name, now, false)
            val s = neo4jQuerier.createBrowseNode(cn)
            if(s)log.info(s"CC_T,${cn.grownOnString},${cn.id},${cn.name}")
            (cn,s)
          }
        }
      }.filter{t=> t._2}.map{t=>t._1}

      if(!children.isEmpty){
        neo4jQuerier.createChildRelationships(node, children, true)
        children.foreach{child =>
          if(!child.growthCompleted) {
            val bnlur = BrowseNodeLookupRequest(child.id, productAdCreds, Locale('US))
            sender ! BrowseNodeLookupRequestMessage(bnlur, self)
          }
        }
      }

      val ancestors = bn.ancestors.getOrElse(List.empty[BrowseNode]).map{ancestor =>
        neo4jQuerier.findNode(ancestor.id) match{
          case Some(an) => (an,true)
          case _ => {
            val an = Neo4jBrowseNode(ancestor.id, ancestor.name, now, false)
            val s = neo4jQuerier.createBrowseNode(an)
            (an,s)
          }
        }
      }.filter{t=>t._2}.map{t=>t._1}
      if(!ancestors.isEmpty)neo4jQuerier.createParentRelationships(node, ancestors, true)
      //TODO send messages to request handler to look up ancestor nodes
    }


    case BrowseNodeLookupFailureMessage(bnlr, rc, error) =>{
        if(rc<maximumRequestAttempts)sender ! BrowseNodeLookupRequestMessage(bnlr, self, rc+1)
      else log.error(s"E,${ErrorCodes.MAX_ATTEMPTS_EXCEEDED},${bnlr.browseNodeId}")
    }

    case TimedShutdownMessage(time) => {
      log.info("GOT A BROWSENODE SHUTDOWN MESSAGE")
      val now = Platform.currentTime
      lastStorageTime match{
        case Some(lst) =>{
          if((now-lst)>shutdownTimeoutMillis) {
            log.info("CALLING SHUTDOWN")
            appShutdown()
          }else system.scheduler.scheduleOnce(
            Duration.create(shutdownTimeoutMillis, TimeUnit.MILLISECONDS),
            self,
            TimedShutdownMessage(now))
        }
        case _ => {
          lastStorageTime = Some(now)
          system.scheduler.scheduleOnce(
            Duration.create(shutdownTimeoutMillis, TimeUnit.MILLISECONDS),
            self,
            TimedShutdownMessage(now))
        }
      }
    }
      
    case ApplicationShutdown(force) => {
      log.info("ProductAd Response Handler received shutdown message")
      log.info("Sending Application shutdown")
      //context.system.shutdown()
      appShutdown()
    }

    case msg:Any => sender ! UnhandledMessageType(s"Unknown msg type ${msg}")

  }

}

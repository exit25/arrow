package com.exit25.arrow.crawler.handlers

import akka.actor.ActorRef
import com.exit25.arrow.mws.AWSProduct

/**
 * Created by masinoa on 1/31/16.
 */
trait MWSRequestMessageWorker {
  def sendResponse(mrm:MWSRequestMessage, status:MWSMessageStatus, task:MWSRequestTask, products:List[AWSProduct],
                   mrm_rh:ActorRef, recipient:ActorRef, rc:Int, sliceStart:Option[Int]=None,
                   checkAsins:Boolean=false) : Option[String] = {
    val (slice, start) = sliceStart match{
      case Some(s) => (true, s)
      case _ => (false, 0)
    }
    mrm match{
      case r:MWSASINsRequest => {
        if(slice) recipient ! MWSASINsRequest(r.asins.slice(start, r.asins.length), task, status, mrm_rh, mrm.productCategory, rc, products)
        else {
          if(checkAsins){
            val valid_asins = r.asins.filter(_.length>0)
            val valid_products = products.filter(_.asin().getOrElse("").length>0)
            recipient ! MWSASINsRequest(valid_asins, task, status, mrm_rh, mrm.productCategory, rc, valid_products)
          }
          else recipient ! MWSASINsRequest(r.asins, task, status, mrm_rh, mrm.productCategory, rc, products)
        }
        None
      }
      case r:MWSBookRequest => {
        if(slice) recipient ! MWSBookRequest(r.items.slice(start, r.items.length), task, status, mrm_rh, mrm.productCategory, rc, products)
        else {
          if(checkAsins){
            val valid_items = r.items.filter(_.asin.length>0)
            val valid_products = products.filter(_.asin().getOrElse("").length>0)
            recipient ! MWSBookRequest(valid_items, task, status, mrm_rh, mrm.productCategory, rc, valid_products)
          }
          else recipient ! MWSBookRequest(r.items, task, status, mrm_rh, mrm.productCategory, rc, products)
        }
        None
      }
      case r:MWSElectronicsRequest => {
        if(slice) recipient ! MWSElectronicsRequest(r.items.slice(start, r.items.length), task, status, mrm_rh, mrm.productCategory, rc, products)
        else {
          if(checkAsins){
            val valid_items = r.items.filter(_.asin.length>0)
            val valid_products = products.filter(_.asin().getOrElse("").length>0)
            recipient ! MWSElectronicsRequest(valid_items, task, status, mrm_rh, mrm.productCategory, rc, valid_products)
          }
          recipient ! MWSElectronicsRequest(r.items, task, status, mrm_rh, mrm.productCategory, rc, products)
        }
        None
      }
      case _ => Some(s"""E_MWSRH,${ErrorCodes.UNEXPECTED_MESSAGE_TYPE},$mrm""")
    }
  }

  def sendStorageRequest(mrm:MWSRequestMessage, storageHandler:ActorRef) : Option[String] = {
    mrm match {
      case r:MWSBookRequest => {
        storageHandler ! StoreItemBooksMessage(r.items, r.products)
        None
      }
      case r:MWSElectronicsRequest => {
        storageHandler ! StoreItemElectronicsMessage(r.items, r.products)
        None
      }
      case _ => Some(s"""E_STORAGE,${ErrorCodes.UNEXPECTED_MESSAGE_TYPE},$mrm""")

    }
  }
  
  def asinsFromMessage(mrm:MWSRequestMessage): List[String] = {
    mrm match{
      case r: MWSASINsRequest => r.asins
      case r: MWSBookRequest => r.items.map{_.asin}
      case r: MWSElectronicsRequest => r.items.map{_.asin}
      case _ => List.empty[String]
    }
  }
}
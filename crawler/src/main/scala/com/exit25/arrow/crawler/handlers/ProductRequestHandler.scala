package com.exit25.arrow.crawler.handlers

import java.util.concurrent.TimeUnit

import akka.actor.{ActorRef, Actor, ActorSystem}
import akka.event.{Logging, LoggingAdapter}
import com.exit25.arrow.productAd.http.HTTPService
import com.exit25.arrow.productAd.indicies.{Electronics, Books}
import com.exit25.arrow.productAd.model.{ItemElectronic, ItemRequestInfo, ItemBook, BrowseNode}
import com.exit25.arrow.productAd.requests.ItemSearchRequest
import com.typesafe.config.Config

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.{Duration}


/**
 * Created by masinoa on 11/15/15.
 */
class ProductRequestHandler(config: Config, dbStorage : ActorRef) extends Actor{

  val system = ActorSystem("ProductRequestHandler")
  val log: LoggingAdapter = Logging(context.system, this)
  val requestDelta = config.getInt("requestDelta")
  val maxPageCount = config.getInt("maxPageCount")

  var awaitingResponse = false
  var lastAWSRequestTime = 0L

  def receive = {

    case BrowseNodeLookupRequestMessage(bnlr, rh, rc) => {
      val delta = System.currentTimeMillis() - lastAWSRequestTime
      if(awaitingResponse || delta<=requestDelta){
        system.scheduler.scheduleOnce(
          Duration.create(requestDelta - delta, TimeUnit.MILLISECONDS),
          self,
          BrowseNodeLookupRequestMessage(bnlr, rh, rc))
      }
      else{
        try {
          awaitingResponse = true
          HTTPService.executeRequest(bnlr).map{r =>
            lastAWSRequestTime = System.currentTimeMillis()
            awaitingResponse = false
            BrowseNode.response2BrowseNode(r) match {
              case Some(bn) => rh ! BrowseNodeLookupSuccessMessage(bn)
              case _ => log.error(s"E,${ErrorCodes.BROWSENODE_REQUEST_RESULT_CASE},${bnlr.browseNodeId}")
            }
          }
        } catch {
          case e: Exception => {
            log.error(s"E,${ErrorCodes.BROWSENODE_REQUEST_EXCEPTION},${bnlr.browseNodeId}")
            awaitingResponse = false
            lastAWSRequestTime = System.currentTimeMillis()
            rh ! BrowseNodeLookupFailureMessage(bnlr, rc)
          }
        }
      }
    }


    case ItemSearchRequestMessage(isr, rh, rc) =>{
      val delta = System.currentTimeMillis() - lastAWSRequestTime

      if(awaitingResponse || delta<=requestDelta){
        system.scheduler.scheduleOnce(
          Duration.create(requestDelta, TimeUnit.MILLISECONDS),
          self,
          ItemSearchRequestMessage(isr, rh, rc)
        )
      }else{
        try{
          awaitingResponse = true
          HTTPService.executeRequest(isr).map{ r =>
            lastAWSRequestTime = System.currentTimeMillis()
            awaitingResponse = false
            if(isr.queryParams.getOrElse("ItemPage","0")=="1")dbStorage ! StoreRequestQueryMessage(isr)
            val body = r.body

            //convert to object representation based on search index
            isr.searchIndex.name match {
              case Books.name => ItemBook.response2ItemBooks(body) match {
                case Some(l) => {
                  val invalidItems = l.filterNot(_.asin.trim.length>0)
                  if(!invalidItems.isEmpty)log.debug("PRH,RECEIVED ITEM WITH NO ASIN")
                  rh ! BookSearchSuccessMessage(l)
                }
                case _ => log.error(s"E,${ErrorCodes.ITEMSEARCH_REQUEST_RESULT_CASE}, ${isr.queryParams}")
              }
              case Electronics.name => ItemElectronic.response2ItemElectronic(body) match{
                case Some(l) => rh ! ElectronicsSearchSuccessMessage(l)
                case _ => log.error(s"E,${ErrorCodes.ITEMSEARCH_REQUEST_RESULT_CASE}, ${isr.queryParams}")
              }
              case _ => log.error(s"E,${ErrorCodes.ITEMSEARCH_UNKNOWN_SEARCH_INDEX}, ${isr.searchIndex.name}")
            }

            //send further request if more response pages
            ItemRequestInfo.response2ItemRequestInfo(body) match{
              case Some(info) => {
                val page: Int = info.itemPage
                if(page<info.totalPages && page<maxPageCount){
                  val queryParams = isr.queryParams.+("ItemPage"->(page+1))
                  val nextPage = ItemSearchRequest(queryParams, isr.credentials, isr.searchIndex, isr.locale)
                  self ! ItemSearchRequestMessage(nextPage, rh, 0)
                }
              }
              case _ => log.error(s"E,${ErrorCodes.ITEMSEARCH_INFO_ERROR}, ${isr.queryParams}")
            }

          }//end execute request
        }catch{
          case e:Exception =>{
            log.error(s"E,${ErrorCodes.ITEMSEARCH_REQUEST_EXCEPTION},${isr.queryParams}")
            awaitingResponse = false
            lastAWSRequestTime = System.currentTimeMillis()
            rh ! ItemSearchFailureMessage(isr, rc)
          }
        }
      }
    }

    case ApplicationShutdown(force) => {
      log.info("ProductAd Request Handler received shutdown message")
      system.shutdown()
      context.system.shutdown()
    }

    case m:Any => sender ! UnhandledMessageType(s"Unknown msg type $m")

  }//end receive
}



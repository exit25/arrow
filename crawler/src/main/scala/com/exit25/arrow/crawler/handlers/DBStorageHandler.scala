package com.exit25.arrow.crawler.handlers

import java.sql.{BatchUpdateException, Date, Timestamp}
import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorSystem}
import akka.event.{Logging, LoggingAdapter}
import com.exit25.arrow.db.ConnectionFactory
import com.exit25.arrow.mws.AWSProduct
import com.typesafe.config.Config
import slick.lifted.TableQuery

import scala.compat.Platform
import com.exit25.arrow.db.model._
import slick.driver.PostgresDriver.api._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContextExecutor, Future}
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by masinoa on 12/7/15.
 */
class DBStorageHandler(config: Config, appShutdown: ()=> Unit) extends Actor {

  val log: LoggingAdapter = Logging(context.system, this)

  lazy val db = ConnectionFactory.connect2pgsql("db", config)
  val productSearchQueries = TableQuery[ProductSearchesQueries]
  val booksTable = TableQuery[Books]
  val electronicsTable = TableQuery[Electronics]
  //val lowestOffersTable = TableQuery[LowestOfferListings]
  //val competitivePricesTable = TableQuery[CompetitivePrices]
  val system = ActorSystem("DBStorageHandlerSystem")
  val shutdownTimeoutMillis = config.getInt("shutdownTimeoutMinutes")*60*1000L


  var storageIter = 0
  var storageCount = 0
  var productUpdateIter = 0
  var productUpdateCount = 0
  var lastStorageTime : Option[Long] = None
  //var currentBatch = (List.empty[Book], List.empty[CompetitivePrice], List.empty[LowestOfferListing])

  def receive = {
    case StoreRequestQueryMessage(isr) => {
      try {
        val now = new Timestamp(Platform.currentTime)
        val kw = isr.queryParams.get("Keywords") match {
          case Some(s: String) => Some(s)
          case _ => None
        }
        val bn = isr.queryParams.get("BrowseNode") match {
          case Some(s: String) => Some(s)
          case _ => None
        }
        val psq = ProductSearchQuery(None, isr.searchIndex.name, kw, bn, now)
        val up = productSearchQueries.insertOrUpdate(psq)
        db.run(up)
      } catch {
        case e: Exception => {
          log.error(e, s"DB_SH,SRQM,${e.getMessage}")
        }
      }

    }

    case StoreProductUpdateMessage(awsProducts, productCategory) => {
      val now = Platform.currentTime
      lastStorageTime = Some(now)
      val ts = new Date(now)
      // check that all necessary tables exist before write
      val f = CompetitivePricesTableQuery.createIfNotExists(productCategory, ts, db).andThen { case s1 =>
        LowestOfferListingsTableQuery.createIfNotExists(productCategory, ts, db).andThen { case s2 =>
          SalesRanksTableQuery.createIfNotExists(productCategory, ts, db).andThen { case s3 =>
            //get the tables
            val competitivePricesTable = CompetitivePricesTableQuery(ts, productCategory)
            val lowestOffersTable = LowestOfferListingsTableQuery(ts, productCategory)
            val salesRankTable = SalesRanksTableQuery(ts, productCategory)
            //get the new rows
            val compPrices = compPricesFromAWSProducts(ts, awsProducts)
            val lowOffers = lowestOffersFromAWSProducts(ts, awsProducts)
            val salesRanks = salesRankFromAWSProducts(ts, awsProducts)
            //set up new row insertions
            val insertOps = DBIO.seq(
              competitivePricesTable ++= compPrices,
              lowestOffersTable ++= lowOffers,
              salesRankTable ++= salesRanks
            )

            try {
              db.run(insertOps)
              productUpdateIter = productUpdateIter + 1
              productUpdateCount = productUpdateCount + awsProducts.length
              log.info(s"iteration: $productUpdateIter, count=$productUpdateCount: insert ops successful for ${awsProducts.head.asin}")
            } catch {
              case bue: BatchUpdateException => {
                log.error(bue.getNextException.getMessage)
              }
              case e: Exception => {
                log.error(s"Storage exception (1): $storageIter, count=$storageCount ${e.getMessage}")
              }
            }
          }
        }
      }
      Await.result(f, Duration.create(10, TimeUnit.SECONDS))
    }

    case StoreItemElectronicsMessage(items, awsProducts) =>{
      val now = Platform.currentTime
      lastStorageTime = Some(now)
      val ts = new Date(now)
      try {

        //val valid_items = items.filter(_.asin.length>0)
        val valid_items = items.filter{_.asin.length>0}
        val asins = valid_items.map{_.asin}

        val qry = electronicsTable.filter(b=> b.asin inSet asins).map(_.asin)

        val f0 = db.run(qry.result)


        val f = f0.andThen {case s0 =>
          s0.map{case s =>
            //PRODUCT ASINs ALREADY IN DB
            val asinsToFilter = (List.empty[String]/:s){(l,x) => x::l}

            //WE ONLY WANT TO STORE PRODUCTS NOT CURRENTLY IN THE DB
            val filteredAWSProducts = awsProducts.filterNot{p=>asinsToFilter.contains(p.asin.getOrElse("UNK"))}

            val electronics: List[Electronic] = valid_items.filterNot{i=>asinsToFilter.contains(i.asin)}.map { i =>
              val aou = if (i.allOffersUrl.length > 0) Some(i.allOffersUrl) else None
              val bind = if (i.binding.length == 0) None else Some(i.binding)
              Electronic(i.asin, i.upc, i.brand, i.manufacturer, i.title,
                bind, i.detailPageUrl, aou, i.browseNodeId, i.browseNodeName,
                i.weight, i.weightUnits, i.length, i.height, i.width, i.lengthUnits, ts)
            }

            //make sure necessary tables exist
            CompetitivePricesTableQuery.createIfNotExists(ElectronicsCategory, ts, db).andThen{case s1 =>
              LowestOfferListingsTableQuery.createIfNotExists(ElectronicsCategory, ts, db).andThen{case s2 =>
                SalesRanksTableQuery.createIfNotExists(ElectronicsCategory, ts, db).andThen{case s3 =>
                  //get the tables
                  val competitivePricesTable = CompetitivePricesTableQuery(ts, ElectronicsCategory)
                  val lowestOffersTable = LowestOfferListingsTableQuery(ts, ElectronicsCategory)
                  val salesRankTable = SalesRanksTableQuery(ts, ElectronicsCategory)
                  //get the new rows
                  val compPrices = compPricesFromAWSProducts(ts, filteredAWSProducts)
                  val lowOffers = lowestOffersFromAWSProducts(ts, filteredAWSProducts)
                  val salesRanks = salesRankFromAWSProducts(ts, filteredAWSProducts)
                  //setup insert ops
                  val insertOps = DBIO.seq(
                    electronicsTable ++= electronics,
                    competitivePricesTable ++= compPrices,
                    lowestOffersTable ++= lowOffers,
                    salesRankTable ++= salesRanks
                  )
                  //run the insert
                  try {
                    db.run(insertOps)
                    storageIter = storageIter + 1
                    storageCount = storageCount + electronics.length
                    log.info(s"Electronics Storage iteration: $storageIter, count=$storageCount: insert ops successful for ${valid_items.head.asin}")
                  } catch {
                    case bue: BatchUpdateException => {
                      log.error(bue.getNextException.getMessage)
                    }
                    case e: Exception => {
                      log.error(s"Electronics Storage exception (1): $storageIter, count=$storageCount ${e.getMessage}")
                    }
                  }
                }
              }
            }

          }
        }//END f

        Await.result(f, Duration.create(10, TimeUnit.SECONDS))

      } catch {
        case e: Exception => {
          log.error(e, s"E, SIM, ${e.getMessage}")
        }
      }
    }

    case StoreItemBooksMessage(items, awsProducts) => {
      //println(s"Store Item Book Message: ${items.length}")
      val now = Platform.currentTime
      lastStorageTime = Some(now)
      val ts = new Date(now)
      try {

        val asins = items.map{_.asin}

        val qry = booksTable.filter(b=> b.asin inSet asins).map(_.asin)

        val f0 = db.run(qry.result)

        val f = f0.andThen{case s0 =>
          s0.map{case s =>
            //PRODUCT ASINs ALREADY IN DB
            val asinsToFilter = (List.empty[String]/:s){(l,x) => x::l}

            //WE ONLY WANT TO STORE PRODUCTS NOT CURRENTLY IN THE DB
            val filteredAWSProducts = awsProducts.filterNot{p=>asinsToFilter.contains(p.asin.getOrElse("UNK"))}

            val books: List[Book] = items.filterNot{i=>asinsToFilter.contains(i.asin)}.map { i =>
              val aou = if (i.allOffersUrl.length > 0) Some(i.allOffersUrl) else None
              val ed = if (i.edition == 0) None else Some(i.edition)
              val bind = if (i.binding.length == 0) None else Some(i.binding)
              val pub = if (i.publisher.length == 0) None else Some(i.publisher)
              val pubYear = if (i.publicationYear == 0) None else Some(i.publicationYear)
              Book(i.asin, i.browseNodeId, i.browseNodeName,
                i.detailPageURL, aou,
                i.title, i.author, ed, i.isbn, bind, pub, pubYear,
                i.height, i.length, i.width, i.lengthUnits,
                i.weight, i.weightUnits,
                ts)
            }

            //println(s"books is ${books.length}")

            //make sure necessary tables exist
             CompetitivePricesTableQuery.createIfNotExists(BooksCategory, ts, db).andThen{case s1 =>
              LowestOfferListingsTableQuery.createIfNotExists(BooksCategory, ts, db).andThen{case s2 =>
                SalesRanksTableQuery.createIfNotExists(BooksCategory, ts, db).andThen{case s3 =>
                  //get the tables
                  val competitivePricesTable = CompetitivePricesTableQuery(ts, BooksCategory)
                  val lowestOffersTable = LowestOfferListingsTableQuery(ts, BooksCategory)
                  val salesRankTable = SalesRanksTableQuery(ts, BooksCategory)
                  //get the new rows
                  val compPrices = compPricesFromAWSProducts(ts, filteredAWSProducts)
                  val lowOffers = lowestOffersFromAWSProducts(ts, filteredAWSProducts)
                  val salesRanks = salesRankFromAWSProducts(ts, filteredAWSProducts)

                  val insertOps = DBIO.seq(
                    booksTable ++= books,
                    competitivePricesTable ++= compPrices,
                    lowestOffersTable ++= lowOffers,
                    salesRankTable ++= salesRanks
                  )

                  try {
                    val x: Future[Unit] = db.run(insertOps)
                    storageIter = storageIter + 1
                    storageCount = storageCount + books.length
                    //println(s"Books Storage iteration: $storageIter, count=$storageCount: insert ops successful for ${items.head.asin}")
                    log.info(s"Books Storage iteration: $storageIter, count=$storageCount: insert ops successful for ${items.head.asin}")
                  } catch {
                    case bue: BatchUpdateException => {
                      log.error(bue.getNextException.getMessage)
                    }
                    case e: Exception => {
                      log.error(s"Books Storage exception (1): $storageIter, count=$storageCount ${e.getMessage}")
                    }
                  }
                }
              }
            }

          }
        }//END f

        Await.result(f, Duration.create(120, TimeUnit.SECONDS))

      } catch {
        case e: Exception => {
          log.error(e, s"E, SIM, ${e.getMessage}")
        }
      }
    }//end case Store ItemsMessage

    case TimedShutdownMessage(time) =>{
      log.info("GOT A STORAGE SHUTDOWN MESSAGE")
      val now = Platform.currentTime
      lastStorageTime match{
        case Some(lst) =>{
          if((now-lst)>shutdownTimeoutMillis) {
            log.info("CALLING SHUTDOWN")
            appShutdown()
          }else system.scheduler.scheduleOnce(
            Duration.create(shutdownTimeoutMillis, TimeUnit.MILLISECONDS),
            self,
            TimedShutdownMessage(now))
        }
        case _ => {
          lastStorageTime = Some(now)
          system.scheduler.scheduleOnce(
            Duration.create(shutdownTimeoutMillis, TimeUnit.MILLISECONDS),
            self,
            TimedShutdownMessage(now))
        }
      }
    }

    case ApplicationShutdown(force) => {
      log.info("DBStorageHandler received shutdown message")
      context.system.shutdown()
    }

    case m:Any => sender ! UnhandledMessageType(m)
  } // end receive

  def compPricesFromAWSProducts(ts:Date, awsproducts:List[AWSProduct]): List[CompetitivePrice] = {
    val compPrices: List[CompetitivePrice] = for {
      p <- awsproducts
      asin <- p.asin()
    } yield {
        CompetitivePrice(None, asin, p.tradeInValue, p.buyBoxNewListPrice, p.buyBoxNewLandedPrice(),
          p.buyBoxUsedListPrice, p.buyBoxUsedLandedPrice(), ts)
    }
    compPrices
   }

  def salesRankFromAWSProducts(ts:Date, awsproducts:List[AWSProduct]): List[SalesRank] = {
    val salesRanks : List[SalesRank] = for{
      p <- awsproducts
      asin <- p.asin()
      sr <- p.displaySalesRank()
    } yield{
      SalesRank(None, asin, sr, ts)
    }
    salesRanks
  }

  def lowestOffersFromAWSProducts(ts:Date, awsproducts:List[AWSProduct]): List[LowestOfferListing] = {
    val lowOffers: List[LowestOfferListing] = (for {
      p <- awsproducts
      asin <- p.asin
      lol <- p.lowestOfferListings()
    } yield {
        for {
          lo <- lol
          landedPrice <- lo.landedPrice
          listPrice <- lo.listPrice
          ship <- lo.shipping
          fullfillment <- lo.fulfillment
          cond <- lo.condition
        } yield {
          val b = lo.multipleOffersAtLowestPrice match {
            case Some(s) => {
              val sl = s.toLowerCase
              if (sl == "false" || sl == "true") Some(sl.toBoolean)
              else None
            }
            case _ => None
          }
          LowestOfferListing(None, asin, landedPrice, listPrice, ship, fullfillment,
            cond, lo.subCondition, lo.sellerFeedback, b, ts)
        }
      }).flatten
    lowOffers
  }
}

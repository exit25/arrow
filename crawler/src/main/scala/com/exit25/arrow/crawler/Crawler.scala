package com.exit25.arrow.crawler

import java.io.File

import akka.actor.{ActorSystem, Props}
import com.exit25.arrow.crawler.handlers._
import com.exit25.arrow.neo4j.{BrowseNodeQuerier, ConnectionFactory}
import com.exit25.arrow.productAd.auth.AWSKeyIdentifiers
import com.exit25.arrow.productAd.indicies.{Books, Electronics}
import com.exit25.arrow.productAd.locales.Locale
import com.exit25.arrow.productAd.requests.{BrowseNodeLookupRequest, ItemSearchRequest}
import com.typesafe.config.ConfigFactory
import play.api.libs.openid.Errors.AUTH_CANCEL

import scala.collection.JavaConversions._
import scala.collection.SortedMap
import scala.compat.Platform
import scala.concurrent.ExecutionContextExecutor


/**
 * Created by masinoa on 10/31/15.
 */

object Crawler {

  def main(args : Array[String]) : Unit = {
    val appNum = if(args.length<1){
      readApplicationNumberFromUser()
    }else{
      processSelection(args(0)) match{
        case Right(i) => i
        case Left(s) => {
          println(s)
          readApplicationNumberFromUser()
        }
      }
    }

    if(appNum==0) System.exit(0)
    else if(appNum==1) {
      println("Running BrowseNode Crawler Application ...")
      crawlBrowseNodes()
    }else if(appNum==2){
      println("Running Product Crawler Using BrowseNode Leaves")
      crawlProductsByBrowseNode()
    }else if(appNum==3){
      println("Running Product Crawler Using Keyword Files")
      crawlProductsByKeywordFile()
    }else if(appNum==4){
      println("Running Product Update Application")
      productUpdate()
    }else if(appNum==5){
      println("Running BrowseNode Crawler -> Products by Browsenode -> Products by File")
      crawlBrowseNodes(1)
    }

  }

  def productUpdate() = {
    val conf = ConfigFactory.parseFile(new File("./conf/application.conf"))
    val crawlerConfig = conf.getConfig("arrow.crawler")

    val dbStorageSystem = ActorSystem("DBStorageHandlers", crawlerConfig)
    val mwsRequestHandlerSystem = ActorSystem("MWSRequestHandlers", crawlerConfig)
    val productUpdateHanderSystem = ActorSystem("ProductUpdateHandlers", crawlerConfig)

    val dbStorageActor = dbStorageSystem.actorOf(
      Props(new DBStorageHandler(crawlerConfig.getConfig("dbStorageHandler"),
        shutdown _)),
      name="dbStorageHandler")

    val mwsRequestHanlderActor = mwsRequestHandlerSystem.actorOf(
      Props(new MWSRequestHandler(crawlerConfig.getConfig("mwsRequestHandler"))),
      name="mwsRequestHandler"
    )

    val productUpdateHandlerActor = productUpdateHanderSystem.actorOf(
      Props(new ProductUpdateHandler(crawlerConfig.getConfig("productUpdateHandler"),
      dbStorageActor,
      mwsRequestHanlderActor)),
      name = "productUpdateHandler"
    )

    productUpdateHandlerActor ! UpdateBooksMessage
    productUpdateHandlerActor ! UpdateElectronicsMessage
    dbStorageActor ! TimedShutdownMessage(Platform.currentTime)

  }

  def crawlProductsByKeywordFile() = {
    val conf = ConfigFactory.parseFile(new File("./conf/application.conf"))
    val crawlerConfig = conf.getConfig("arrow.crawler")

    val dbStorageSystem = ActorSystem("DBStorageHandlers", crawlerConfig)
    val productAdRequestHandlerSystem = ActorSystem ("ProductAdRequestHandlers", crawlerConfig)
    val mwsRequestHandlerSystem = ActorSystem("MWSRequestHandlers", crawlerConfig)
    val itemResponseSystem = ActorSystem("ItemResponseHandlers", crawlerConfig)

    val dbStorageActor = dbStorageSystem.actorOf(
      Props(new DBStorageHandler(crawlerConfig.getConfig("dbStorageHandler"),
      shutdown _)),
      name="dbStorageHandler")

    val productAdRequestHandlerActor = productAdRequestHandlerSystem.actorOf (
      Props(new ProductRequestHandler(crawlerConfig.getConfig("productRequestHandler"),dbStorageActor)),
      name="productRequestHandler")

    val mwsRequestHanlderActor = mwsRequestHandlerSystem.actorOf(
      Props(new MWSRequestHandler(crawlerConfig.getConfig("mwsRequestHandler"))),
      name="mwsRequestHandler"
    )

    val itemResponseActor = itemResponseSystem.actorOf(
      Props(new ItemResponseHandler(
        crawlerConfig.getConfig("itemResponseHandler"),
        dbStorageActor,
        mwsRequestHanlderActor
      )),
      name="itemResponseHandler"
    )

    val prodCreds = AWSKeyIdentifiers.credsFromConfig(crawlerConfig.getConfig("browseNodeResponseHandler.credentials"))

    val keyWordFiles = crawlerConfig.getConfig("keywordFiles")
    val rootDir = keyWordFiles.getString("dir")
    keyWordFiles.getConfigList("items").foreach{c =>

      val file = c.getString("file")

      val searchIndex = Symbol(c.getString("searchIndex")) match{
        case 'Books => Books(Locale('US))
        case 'Electronics => Electronics(Locale('US))
        case _ => Books(Locale('US))
      }

      val sortOptions = c.getStringList("sort")

      val browseNodeId = c.getString("browseNode")
      //val stream  = getClass.getResourceAsStream(s"$rootDir/$file")
      val keywords = scala.io.Source.fromFile(new File(s"$rootDir/$file")).getLines
      var count = 0
      keywords.foreach{kw =>
        sortOptions.foreach{so =>
          val queryParams = SortedMap(
            "BrowseNode"->browseNodeId,
            "Keywords"->kw.trim,
            "ItemPage"->"1",
            "ResponseGroup"->"ItemAttributes, SalesRank, BrowseNodes",
            "Sort"->so
          )
          val request = ItemSearchRequest(queryParams, prodCreds, searchIndex, Locale('US))
          val message = ItemSearchRequestMessage(request, itemResponseActor, 0)
          count = count + 1
          println(s"Sending ItemSearchRequest for keyword:$kw, count:$count")
          productAdRequestHandlerActor ! message
        }//end sort options foreach
      }
    }

    dbStorageActor ! TimedShutdownMessage(Platform.currentTime)

  }

  def crawlProductsByBrowseNode(shutdownChoice : Int = 0)() = {
    val conf = ConfigFactory.parseFile(new File("./conf/application.conf"))
    val crawlerConfig = conf.getConfig("arrow.crawler")

    val dbStorageSystem = ActorSystem("DBStorageHandlers", crawlerConfig)
    val productAdRequestHandlerSystem = ActorSystem ("ProductAdRequestHandlers", crawlerConfig)
    val mwsRequestHandlerSystem = ActorSystem("MWSRequestHandlers", crawlerConfig)
    val itemResponseSystem = ActorSystem("ItemResponseHandlers", crawlerConfig)

    val shutdownFunc = if(shutdownChoice==0) shutdown _
                       else crawlProductsByKeywordFile _

    val dbStorageActor = dbStorageSystem.actorOf(
      Props(new DBStorageHandler(crawlerConfig.getConfig("dbStorageHandler"),
      shutdownFunc)),
      name="dbStorageHandler")

    val productAdRequestHandlerActor = productAdRequestHandlerSystem.actorOf (
      Props(new ProductRequestHandler(crawlerConfig.getConfig("productRequestHandler"),dbStorageActor)),
      name="productRequestHandler")

    val mwsRequestHanlderActor = mwsRequestHandlerSystem.actorOf(
      Props(new MWSRequestHandler(crawlerConfig.getConfig("mwsRequestHandler"))),
      name="mwsRequestHandler"
    )
    val itemResponseActor = itemResponseSystem.actorOf(
      Props(new ItemResponseHandler(
        crawlerConfig.getConfig("itemResponseHandler"),
        dbStorageActor,
        mwsRequestHanlderActor
      )),
      name = "itemResponseActor"
    )

    implicit val neo4jConf = ConnectionFactory.apply(crawlerConfig.getConfig("browseNodeResponseHandler.neo4j"))
    implicit val ec: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global
    val neo4jQuerier = BrowseNodeQuerier(neo4jConf, ec)
    val prodCreds = AWSKeyIdentifiers.credsFromConfig(crawlerConfig.getConfig("browseNodeResponseHandler.credentials"))
    val browseNodeSeeds = crawlerConfig.getConfig("browseNodeSeeds")
    browseNodeSeeds.getStringList("searchIndices").foreach{si =>
      val sis = Symbol(si)
      val searchIndex = sis match{
        case 'Books => Books(Locale('US))
        case 'Electronics => Electronics(Locale('US))
        case _ => Books(Locale('US))
      }
      val sortOptions = browseNodeSeeds.getStringList(s"${si}Sort")
      browseNodeSeeds.getStringList(si).foreach{id =>
        neo4jQuerier.findNode(id) match{
          case Some(bn) =>{
            val leavesOnly = try{ browseNodeSeeds.getBoolean(s"${si}LeavesOnly")}catch {
              case e: Exception => false
            }

            if(leavesOnly){
              neo4jQuerier.findLeafNodes(bn) match{
                case Some(descendants) =>{
                  var nodeCount = 0
                  descendants.foreach { dcendnt=>
                    sortOptions.foreach{so =>
                      val queryParams = SortedMap(
                        "BrowseNode"->dcendnt.id,
                        "ItemPage"->"1",
                        "ResponseGroup"->"ItemAttributes, SalesRank, BrowseNodes",
                        "Sort"->so
                      )
                      val request = ItemSearchRequest(queryParams, prodCreds, searchIndex, Locale('US))
                      val message = ItemSearchRequestMessage(request, itemResponseActor, 0)
                      println(s"Sending Leaf Node ItemSearchRequest for seed name:${dcendnt.name} id:$id, child:${dcendnt.id}, count:$nodeCount")
                      nodeCount = nodeCount + 1
                      productAdRequestHandlerActor ! message
                    }//end sortoptions foreach
                  }//end decendants for each
                }
                case _ => println(s"WARNING: NO DESCENDANT NODES FOR BROWSENODE ID:$id")
              }
            }else{
              neo4jQuerier.findDescendants(bn) match{
                case Some(descendants) =>{
                  var nodeCount = 0
                  descendants.foreach { dcendnt=>
                    sortOptions.foreach{so =>
                      val queryParams = SortedMap(
                        "BrowseNode"->dcendnt.id,
                        "ItemPage"->"1",
                        "ResponseGroup"->"ItemAttributes, SalesRank, BrowseNodes",
                        "Sort"->so
                      )
                      val request = ItemSearchRequest(queryParams, prodCreds, searchIndex, Locale('US))
                      val message = ItemSearchRequestMessage(request, itemResponseActor, 0)
                      println(s"Sending Descendent ItemSearchRequest for seed name:${dcendnt.name} id:$id, child:${dcendnt.id}, count:$nodeCount")
                      nodeCount = nodeCount + 1
                      productAdRequestHandlerActor ! message
                    }//end sortoptions foreach
                  }//end decendants for each
                }
                case _ => println(s"WARNING: NO DESCENDANT NODES FOR BROWSENODE ID:$id")
              }
            }
          }
          case _ => println(s"WARNING: NO BROWESNODE FOUND FOR ID:$id")
        }
      }
    }

    dbStorageActor ! TimedShutdownMessage(Platform.currentTime)

  }//end crawl products by browse node

  def crawlBrowseNodes(shutdownChoice:Int=0) = {
    val conf = ConfigFactory.parseFile(new File("./conf/application.conf"))
    val crawlerConfig = conf.getConfig("arrow.crawler")

    val dbStorageSystem = ActorSystem("DBStorageHandlers", crawlerConfig)
    val productAdRequestHandlerSystem = ActorSystem ("ProductAdRequestHandlers", crawlerConfig)
    val browseNodeResponseHandlerSystem = ActorSystem("BrowseNodeResponseHandlers", crawlerConfig)

    val shutdownFunc = if(shutdownChoice==0) shutdown _
                       else crawlProductsByBrowseNode(1) _

    val dbStorageActor = dbStorageSystem.actorOf(
      Props(new DBStorageHandler(crawlerConfig.getConfig("dbStorageHandler"),
      shutdownFunc)),
      name="dbStorageHandler")

    val productAdRequestHandlerActor = productAdRequestHandlerSystem.actorOf (
      Props(new ProductRequestHandler(crawlerConfig.getConfig("productRequestHandler"),dbStorageActor)),
      name="requestHandler")

    val browseNodeResponseHandlerActor = browseNodeResponseHandlerSystem.actorOf(
      Props(new BrowseNodeResponseHandler(crawlerConfig.getConfig("browseNodeResponseHandler"),
        shutdownFunc)),
      name = "browseNodeResponseHandler")

    browseNodeResponseHandlerActor ! DeleteAllBrowseNodesMessage(Platform.currentTime)

    val creds = AWSKeyIdentifiers.credsFromConfig(crawlerConfig.getConfig("browseNodeResponseHandler.credentials"))

    val browseNodeSeeds = crawlerConfig.getConfig("browseNodeSeeds")

    browseNodeSeeds.getStringList("searchIndices").foreach{si =>
      browseNodeSeeds.getStringList(si).foreach{seed =>
        val request = BrowseNodeLookupRequest(seed, creds, Locale('US))
        val message = BrowseNodeLookupRequestMessage(request, browseNodeResponseHandlerActor)
        productAdRequestHandlerActor ! message
      }
    }

    browseNodeResponseHandlerActor ! TimedShutdownMessage(Platform.currentTime)

  }

  def shutdown(){
    println("Shutdown called")
    System.exit(0)
  }

  def processSelection(selection: String) : Either[String, Int] = {
    try{
      val i = selection.trim.toInt
      if(i > -1 && i<6)Right(i)
      else Left("Selection must be an integer in [0,5]")
    }catch{
      case nfe: NumberFormatException => Left("Selection must be an integer")
      case e : Exception => Left("Please enter a valid selection")
    }
  }

  def readApplicationNumberFromUser() : Int = {

    println("Please select the application you wish to run:")
    println("[0] Quit Application")
    println("[1] BrowseNode Crawler")
    println("[2] Product Crawler Using BrowseNodes")
    println("[3] Product Crawler Using Keyword Files")
    println("[4] Update Existing Products Price History")
    println("[5] BrowseNode->BrowseNodes Product Search->File Product Search")
    processSelection(scala.io.StdIn.readLine()) match{
      case Right(i) => i
      case Left(s) => {
        println(s)
        readApplicationNumberFromUser()
      }
    }
  }
}

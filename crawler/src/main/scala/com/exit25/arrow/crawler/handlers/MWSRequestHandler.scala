package com.exit25.arrow.crawler.handlers

import java.util.concurrent.TimeUnit

import akka.actor.{ActorSystem, Actor}
import akka.event.{Logging}
import com.exit25.arrow.mws.AWSProduct
import com.exit25.arrow.mws.auth.AWSSellerCredentials
import com.exit25.arrow.mws.pipeline.sources.{GetLowestOfferListingsForASIN, GetCompetitivePricingForASIN}
import com.typesafe.config.Config

import scala.compat.Platform
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by masinoa on 12/1/15.
 */
class MWSRequestHandler(config:Config) extends Actor with MWSRequestMessageWorker{

  val system = ActorSystem("MWSRequestHandler")
  val log = Logging(context.system, this)
  val lowestOfferListingRequestDelta = config.getInt("throttle.lowestOfferListingRequestDelta")
  val getCompetitivePricingRequestDelta = config.getInt("throttle.getCompetitivePricingRequestDelta")
  val maxASINsPerRequest = config.getInt("maxAsinsPerRequest")

  var lastLowestOfferListingRequestTime = 0L
  var lastGetCompetitivePricingRequestTime = 0L

  val marketPlaceId = config.getString("marketplaceID")
  val creds = AWSSellerCredentials(config.getConfig("credentials"))

  def receive = {

    case mrm:MWSRequestMessage => {
      lazy val asins = asinsFromMessage(mrm)

      val now = System.currentTimeMillis()
      val (delta,validRequestDelta, updateDelta) = mrm.task match {
        case MWSCompPrice => {
          val d = now-lastGetCompetitivePricingRequestTime
          val b = d > getCompetitivePricingRequestDelta
          val ud = getCompetitivePricingRequestDelta - d
          (d,b,ud)
        }
        case MWSLowOffList => {
          val d = now - lastLowestOfferListingRequestTime
          val b = d > lowestOfferListingRequestDelta
          val ud = lowestOfferListingRequestDelta - d
          (d,b, ud)
        }
        case _ => (0,false,0L)
      }

      if(validRequestDelta){
        val lim = math.min(maxASINsPerRequest,asins.length)
        mrm.task match {
          case MWSCompPrice => {
            try{
              val request = GetCompetitivePricingForASIN(marketPlaceId, creds, Some(asins.slice(0,lim)))
              val products: List[AWSProduct] = request.products()
              lastGetCompetitivePricingRequestTime = Platform.currentTime
              val rh = mrm.responseHandler
              val rc = mrm.requestCount
              sendResponse(mrm, MWSSuccessStatus, MWSCompPrice, products, self,rh, rc).map{e => log.error(e)}
              if (asins.length>maxASINsPerRequest){
                sendResponse(mrm, MWSRequestStatus, MWSCompPrice, List.empty[AWSProduct], rh, self, rc, Some(maxASINsPerRequest)).map{e=>log.error(e)}
              }
            }catch{
              case e:Exception =>{
                log.error(s"E,${ErrorCodes.COMPETITIVE_PRICE_REQUEST_ERROR},${asins}")
                lastGetCompetitivePricingRequestTime = Platform.currentTime
                sendResponse(mrm, MWSFailureStatus, MWSCompPrice, List.empty[AWSProduct], self, mrm.responseHandler, mrm.requestCount).map{e=>log.error(e)}
              }
            }
          }//end MWSCompPrice task

          case MWSLowOffList => {
            try{
              val request = GetLowestOfferListingsForASIN(marketPlaceId, creds, Some(asins.slice(0,lim)))
              val updated_products = request.stageResult(mrm.products.slice(0,lim))
              lastLowestOfferListingRequestTime = Platform.currentTime
              val rh = mrm.responseHandler
              val rc = mrm.requestCount
              sendResponse(mrm, MWSSuccessStatus, MWSLowOffList, updated_products, self, rh, rc).map{e=>log.error(e)}
              if (asins.length>maxASINsPerRequest){
                val prod_slice = mrm.products.slice(maxASINsPerRequest,mrm.products.length)
                sendResponse(mrm, MWSRequestStatus, MWSLowOffList,prod_slice, rh, self, rc, Some(maxASINsPerRequest)).map{e=>log.error(e)}
              }
            }catch{
              case e:Exception =>{
                log.error(s"E,${ErrorCodes.COMPETITIVE_PRICE_REQUEST_ERROR},${asins}")
                lastGetCompetitivePricingRequestTime = Platform.currentTime
                sendResponse(mrm, MWSFailureStatus, MWSLowOffList, mrm.products, self, mrm.responseHandler, mrm.requestCount).map{e=>log.error(e)}
              }
            }
          }//end MWSLowOffList task
        }

      }else{
        system.scheduler.scheduleOnce(
        Duration.create(updateDelta, TimeUnit.MILLISECONDS),
        self,
        mrm
        )
      }
    }

    case m:Any => sender ! UnhandledMessageType(m)

  }

}

package com.exit25.arrow.crawler.handlers

import akka.actor.{Actor, ActorRef}
import akka.event.Logging
import com.exit25.arrow.db.ConnectionFactory
import com.exit25.arrow.db.model.{Books, BooksCategory, Electronics, ElectronicsCategory}
import com.typesafe.config.Config
import slick.lifted.{Query, TableQuery}
import slick.driver.PostgresDriver
import slick.driver.PostgresDriver.api._

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by masinoa on 1/23/16.
 */
class ProductUpdateHandler(config:Config, dbStorage: ActorRef, mwsRequestHandler: ActorRef)
  extends Actor with MWSRequestMessageWorker{

  val log = Logging.getLogger(context.system, this)
  val maximumRequestAttempts = config.getInt("maximumRequestAttempts")
  val maximumASINsPerRequest = config.getInt("maxAsinsPerRequest")

  lazy val db = ConnectionFactory.connect2pgsql("db", config)

  def receive = {

    case pum:ProductUpdateMessage => {
      //query the db and generate mws comp price messages
      try{

        val prodCat = pum match {
          case UpdateBooksMessage => BooksCategory
          case UpdateElectronicsMessage => ElectronicsCategory
          case _ => BooksCategory  //this is actually an unknown PUM and is handled below
        }

        val fopt = pum match {
          case UpdateBooksMessage => {
            val table = TableQuery[Books]
            val qry = table.map(_.asin).countDistinct
            Some(db.run(qry.result))
          }
          case UpdateElectronicsMessage =>{
            val table = TableQuery[Electronics]
            val qry = table.map(_.asin).countDistinct
            Some(db.run(qry.result))
          }
          case _ => None
        }

        fopt match{
          case Some(f) => {
            f.onSuccess{case streamTotal =>
              var asins = List.empty[String]
              var streamCount = 0

              val asinStream = pum match{
                case UpdateBooksMessage => {
                  val table = TableQuery[Books]
                  val streamQry = table.map(_.asin)
                  Some(db.stream(streamQry.result))
                }
                case UpdateElectronicsMessage => {
                  val table = TableQuery[Electronics]
                  val streamQry = table.map(_.asin)
                  Some(db.stream(streamQry.result))
                }
                case _ => None
              }

              asinStream.map{stream =>
                stream.foreach{a =>
                  streamCount = streamCount + 1
                  if(asins.length==(maximumASINsPerRequest-1)){
                    asins = a :: asins
                    mwsRequestHandler ! MWSASINsRequest(asins, MWSCompPrice, MWSRequestStatus, self, prodCat)
                    asins = List.empty[String]
                  }else{
                    asins = a :: asins
                  }
                  if(streamCount == streamTotal && !asins.isEmpty){
                    mwsRequestHandler ! MWSASINsRequest(asins, MWSCompPrice, MWSRequestStatus, self, prodCat)
                  }
                }
              }

            }
          }
          case _ => log.error(s"""E_PU,${ErrorCodes.UNEXPECTED_MESSAGE_TYPE},$pum""")
        }
      }catch{
        case e: Exception => log.error(e,s"E, PUM, ${e.getMessage}")
      }
    }

    case mrm:MWSRequestMessage => {
      mrm.status match {
        case MWSFailureStatus => {
          if(mrm.requestCount<maximumRequestAttempts){
            sendResponse(mrm, MWSRequestStatus, mrm.task, mrm.products, self, mwsRequestHandler, mrm.requestCount+1).map{e=>log.error(e)}
          }
          else log.error(s"""PUReq,${ErrorCodes.MAX_ATTEMPTS_EXCEEDED}, $mrm""")
        }
        case MWSSuccessStatus => {
          mrm.task match{
            case MWSCompPrice => {
              sendResponse(mrm, MWSRequestStatus, MWSLowOffList, mrm.products, self, mwsRequestHandler, 0, None, true).map{e=>log.error(e)}
            }
            case MWSLowOffList => {
              dbStorage ! StoreProductUpdateMessage(mrm.products, mrm.productCategory)
            }
          }
        }
        case _ => log.error(s"""PUReq,${ErrorCodes.UNEXPECTED_MESSAGE_TYPE}, $mrm""")
      }
    }

    case m:Any => sender ! UnhandledMessageType(m)


  }

}

package com.exit25.arrow.crawler.handlers

import akka.actor.ActorRef
import com.exit25.arrow.db.model.{Book, CompetitivePrice, LowestOfferListing, ProductCategory}
import com.exit25.arrow.productAd.indicies.SearchIndex
import com.exit25.arrow.productAd.model.{BrowseNode, ItemBook, ItemElectronic}
import com.exit25.arrow.productAd.requests.{BrowseNodeLookupRequest, ItemSearchRequest}
import com.exit25.arrow.mws.AWSProduct

/**
 * Created by masinoa on 11/15/15.
 */
case class ApplicationShutdown(force:Boolean)
case class TimedShutdownMessage(time:Long)


trait Error
case class UnhandledMessageType(msg : Any) extends Error

case class DeleteAllBrowseNodesMessage(time:Long)
case class BrowseNodeLookupRequestMessage(bnlr: BrowseNodeLookupRequest, responseHandler:ActorRef, requestCount:Int=0)
case class BrowseNodeLookupSuccessMessage(bn: BrowseNode)
case class BrowseNodeLookupFailureMessage(bnlr: BrowseNodeLookupRequest, requestCount:Int, error:Int=0)

case class ItemSearchRequestMessage(isr: ItemSearchRequest, responseHandler:ActorRef, requestCount:Int=0)
case class ItemSearchFailureMessage(isr:ItemSearchRequest, requestCount:Int, error:Int=0)

class ItemSearchSuccessMessage
case class BookSearchSuccessMessage(items: List[ItemBook]) extends ItemSearchSuccessMessage
case class ElectronicsSearchSuccessMessage(items:List[ItemElectronic]) extends ItemSearchSuccessMessage

class MWSMessageStatus
object MWSRequestStatus extends MWSMessageStatus
object MWSSuccessStatus extends MWSMessageStatus
object MWSFailureStatus extends MWSMessageStatus

class MWSRequestTask
object MWSCompPrice extends MWSRequestTask
object MWSLowOffList extends MWSRequestTask

class MWSRequestMessage(val task:MWSRequestTask, val status:MWSMessageStatus, val responseHandler:ActorRef,
                        val productCategory: ProductCategory,
                        val requestCount:Int=0,
                        val products:List[AWSProduct]=List.empty[AWSProduct])

case class MWSASINsRequest(asins:List[String], override val task: MWSRequestTask,
                           override val status: MWSMessageStatus,
                           override val responseHandler:ActorRef,
                           override val productCategory: ProductCategory,
                           override val requestCount:Int=0,
                           override val products:List[AWSProduct]=List.empty[AWSProduct]) extends MWSRequestMessage(task, status, responseHandler, productCategory, requestCount, products)

case class MWSBookRequest(items:List[ItemBook], override val task: MWSRequestTask,
                          override val status: MWSMessageStatus,
                          override val responseHandler:ActorRef,
                          override val productCategory: ProductCategory,
                          override val requestCount:Int=0,
                          override val products:List[AWSProduct]=List.empty[AWSProduct]) extends MWSRequestMessage(task, status, responseHandler, productCategory, requestCount, products)

case class MWSElectronicsRequest(items:List[ItemElectronic], override val task: MWSRequestTask,
                                 override val status: MWSMessageStatus,
                                 override val responseHandler:ActorRef,
                                 override val productCategory: ProductCategory,
                                 override val requestCount:Int=0,
                                 override val products:List[AWSProduct]=List.empty[AWSProduct]) extends MWSRequestMessage(task, status, responseHandler, productCategory, requestCount, products)


case class StoreRequestQueryMessage(isr: ItemSearchRequest)
case class StoreProductUpdateMessage(awsproducts:List[AWSProduct], productCategory: ProductCategory)

case class StoreItemElectronicsMessage(items:List[ItemElectronic], awsProducts:List[AWSProduct])
case class StoreItemBooksMessage(items:List[ItemBook], awsProducts:List[AWSProduct])

class ProductUpdateMessage
object UpdateBooksMessage extends ProductUpdateMessage
object UpdateElectronicsMessage extends ProductUpdateMessage


package com.exit25.arrow.crawler.handlers

import akka.actor.{Actor, ActorRef}
import akka.event.Logging
import com.exit25.arrow.db.model.{BooksCategory, ElectronicsCategory}
import com.typesafe.config.Config


/**
 * Created by masinoa on 11/29/15.
 */
class ItemResponseHandler(config:Config, dbStorage : ActorRef, mwsRequestHandler : ActorRef) extends Actor with MWSRequestMessageWorker{

  val log = Logging.getLogger(context.system, this)
  val maximumRequestAttempts = config.getInt("maximumRequestAttempts")

  def receive = {

    //these message will come from the product request handler
    case issm:ItemSearchSuccessMessage =>{
      issm match{
        case BookSearchSuccessMessage(items) => {
          val valid_items = items.filter(_.asin.length>0)
          mwsRequestHandler ! MWSBookRequest(valid_items, MWSCompPrice, MWSRequestStatus, self, BooksCategory)
        }
        case ElectronicsSearchSuccessMessage(items) => {
          val valid_items = items.filter(_.asin.length>0)
          mwsRequestHandler ! MWSElectronicsRequest(valid_items, MWSCompPrice, MWSRequestStatus, self, ElectronicsCategory)
        }
        case _ => log.error(s"unknown item search success message type: ${issm}")
      }
    }

    case mrm:MWSRequestMessage => {
      mrm.status match {
        case MWSFailureStatus => {
          if(mrm.requestCount<maximumRequestAttempts){
            sendResponse(mrm, MWSRequestStatus, mrm.task, mrm.products, self, mwsRequestHandler, mrm.requestCount+1).map{e=>log.error(e)}
          }
          else log.error(s"""E_MWSReq,${ErrorCodes.MAX_ATTEMPTS_EXCEEDED}, $mrm""")
        }
        case MWSSuccessStatus => {
          mrm.task match{
            case MWSCompPrice => {
              sendResponse(mrm, MWSRequestStatus, MWSLowOffList, mrm.products, self, mwsRequestHandler, 0, None, true).map{e=>log.error(e)}
            }
            case MWSLowOffList => {
              sendStorageRequest(mrm, dbStorage).map(e=>log.error(e))
            }
          }
        }
        case _ => log.error(s"""E_MWSReq,${ErrorCodes.UNEXPECTED_MESSAGE_TYPE}, $mrm""")
      }
    }

    case ItemSearchFailureMessage(isr, rc, error) =>{
      if(rc<maximumRequestAttempts) sender ! ItemSearchRequestMessage(isr, self, rc+1)
      else log.error(s"E_IS,${ErrorCodes.MAX_ATTEMPTS_EXCEEDED}, ${isr.queryParams}")
    }

    case ApplicationShutdown(force) => {
      log.info("ProductAd Response Handler received shutdown message")
      log.info("Sending Application shutdown")
      context.system.shutdown()
    }

    case m:Any => sender ! UnhandledMessageType(s"Unknown msg type $m")
  }

}

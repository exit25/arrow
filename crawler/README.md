# Arrow Crawler Application

## BrowseNode Crawler Method
<ol type="1">
<li>Obtain seeds from config file</li>
<li>For each seed:
    <ol type="a">
  <li>check database for lastSearchedOn. If out of date (or non-existent), execute a BrowseNodeLookup
    and to obtain all children. STORE CHILDREN IN THE DB WITH INCOMPLETE STATUS.
    For each child, recursively execute a BrowseNodeLookup (STORE CHILDREN IN DATABASE WITH INCOMPLETE STATUS)
     until reaching a BrowseNode with *no children*. Store this BrowseNode name and ID to the DB in the leaf node table.
     Mark it as complete with the current date. As each level of the recursion is completed, the parent BrowseNode is marked Complete.
   </li>
  <li>if lastSearchedOn is not out of date, check completionStatus. If incomplete, obtain children for the seed via 
    a BrowseNodeLookup. For each child, check DB for existence/completion status. If complete, skip it. If incomplete, 
    perform a BrowseNodeLoookup for it's children. Check DB status for existence/completion status. Perform this
    recursively until reaching a BrowseNode with *no children*. Store this BrowseNode name and ID to the DB in the leaf node table.
    Mark it as complete with the current date. As each level of the recursion is completed, the parent BrowseNode is marked Complete.
    </li>
    </ol>
 </li>
</ol>    
    
## Product Crawler

1. Obtain all leaf browse nodes from the DB
2. Perform an ItemSearch request using the ProductAd api - be sure to get all 10 pages if available
3. For each Item, perform an MWS search to get detailed price information - submit in max batches of 10. Store results
to DB.

## ProductAd Request Limits
The Product Advertising API limits requests as described in the excerpt below from [this documentation](http://docs.aws.amazon.com/AWSECommerceService/latest/DG/TroubleshootingApplications.html)

    Each account used to access the Product Advertising API is allowed an initial usage limit of 1 request per second. 
    Each account will receive an additional 1 request per second (up to a maximum of 10) for every $4,600 of shipped 
    item revenue driven in a trailing 30-day period (about $0.11 per minute).


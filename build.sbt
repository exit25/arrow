import Dependencies._

lazy val commonSettings = Seq(
  version := "0.1.0",
  organization := "com.exit25",
  scalaVersion := "2.11.7",
  libraryDependencies := {
    CrossVersion.partialVersion(scalaVersion.value) match {
      // if scala 2.11+ is used, add dependency on scala-xml module
      case Some((2, scalaMajor)) if scalaMajor >= 11 =>
        libraryDependencies.value ++ Seq(
          "org.scala-lang.modules" %% "scala-xml" % "1.0.3"
          //"org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.3"
          //"org.scala-lang.modules" %% "scala-swing" % "1.0.1"
        )
      case _ =>
        // or just libraryDependencies.value if you don't depend on scala-swing
        libraryDependencies.value //:+ "org.scala-lang" % "scala-swing" % scalaVersion.value
    }
  },
  test in assembly :={}
)

lazy val root = (project in file("."))
  .aggregate(db,mws, productAd, crawler,neo4jDB)
  .settings(commonSettings: _*)
  .settings(name := "arrow")
  .settings(assemblyJarName := "com.exit25.arrow.jar")

//-----------------------------------------------------------------------------------------
// POSTGRES DB CLIENT Project
//-----------------------------------------------------------------------------------------
lazy val db = (project in file("db"))
  .settings(commonSettings: _*)
  .settings(name := "db")
  .settings(libraryDependencies ++= arrow_test(arrowScalaTest)
++ arrow_compile(arrowConfig)
++ arrow_compile(arrowSlick)
++ arrow_compile(arrowLogBack)
++ arrow_compile(arrowPostgresql)
++ arrow_compile(arrowSlickHikari)
++ arrow_compile(arrowJavaMail)
++ arrow_compile(arrowJavaActivation))
  .settings(assemblyJarName := "com.exit25.arrow.db.jar")
  .settings(assemblyMergeStrategy in assembly := {
  case "META-INF/MANIFEST.MF" => MergeStrategy.discard
  case "logback.xml" => MergeStrategy.discard
  case _ => MergeStrategy.first
}).settings(mainClass in Compile := Some("com.exit25.arrow.db.apps.Main"))


//-----------------------------------------------------------------------------------------
// Neo4J CLIENT Project
//-----------------------------------------------------------------------------------------
lazy val neo4jDB = (project in file("neo4jdb"))
  .settings(resolvers ++= Seq(
      "anormcypher" at "http://repo.anormcypher.org/",
      "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/"
   ))
  .settings(commonSettings: _*)
  .settings(name := "neo4jdb")
  .settings(libraryDependencies ++= arrow_test(arrowScalaTest)
  ++ arrow_compile(arrowConfig)
  ++ arrow_compile(arrowLogBack)
  ++ arrow_compile(arrowAnormCypher))
  .settings(assemblyJarName := "com.exit25.arrow.neo4jdb.jar")

//-----------------------------------------------------------------------------------------
// AMAZON CRAWLER Project
//-----------------------------------------------------------------------------------------
lazy val crawler = (project in file("crawler"))
  .dependsOn(productAd, mws, neo4jDB, db)
  .settings(commonSettings: _*)
  .settings(name := "crawler")
  .settings(assemblyJarName := "com.exit25.arrow.crawler.jar")
  .settings(libraryDependencies ++= arrow_compile(arrowConfig)
    ++ arrow_test(arrowScalaTest)
    ++ arrow_compile(arrowLogBack)
    ++ arrow_compile(arrowLogBackCore)

    //++ arrow_compile(arrowAkkaActor)
    //++ arrow_compile(arrowAkkaKernel)
    //++ arrow_compile(arrowAkkaRemote)
    //++ arrow_compile(arrowAkkaSlf4j)
    ++ arrow_compile(arrowSlick)
  )
  .settings(assemblyJarName := "com.exit25.arrow.crawler.jar")
  .settings(mainClass in Compile := Some("com.exit25.arrow.crawler.Crawler"))
  .settings(assemblyMergeStrategy in assembly := {
    case "META-INF/MANIFEST.MF" => MergeStrategy.discard
    case "logback.xml" => MergeStrategy.discard
    case _ => MergeStrategy.first
  })

//-----------------------------------------------------------------------------------------
// Product API Project
//-----------------------------------------------------------------------------------------
lazy val productAd = (project in file("productAd"))
  .settings(commonSettings: _*)
  .settings(name := "productAd")
  .settings(libraryDependencies ++= arrow_test(arrowScalaTest)
  ++ arrow_compile(arrowConfig)
  ++ arrow_compile(arrowApachHttp)
  ++ arrow_compile(arrowCommonsCodec)
  ++ arrow_compile(arrowJodaTime)
  ++ arrow_compile(arrowPlay)
  )
  .settings(assemblyJarName := "com.exit25.arrow.productAd.api.jar")

//-----------------------------------------------------------------------------------------
// MWS API Project
//-----------------------------------------------------------------------------------------
lazy val mws = (project in file("mws"))
  .settings(commonSettings: _*)
  .settings(name := "mws")
  .settings(libraryDependencies ++= arrow_test(arrowScalaTest)
    ++ arrow_compile(arrowConfig)
  )
  .settings(assemblyJarName := "com.exit25.arrow.mws.api.jar")
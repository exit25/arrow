import sbt._

object Dependencies{
  def arrow_compile   (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "compile")

  def arrow_provided  (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "provided")
  def arrow_test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")
  def arrow_runtime   (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "runtime")
  def arrow_container (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "container")

  val arrowScalaTest    =   "org.scalatest"               %% "scalatest"        % "2.2.4"
  val arrowConfig       =   "com.typesafe"                % "config"            % "1.3.0"
  val arrowLogBack      =   "ch.qos.logback"              % "logback-classic"   % "1.1.3"
  val arrowLogBackCore  =   "ch.qos.logback"              % "logback-core"      % "1.1.3"
  val arrowSlick        =   "com.typesafe.slick"          %% "slick"            % "3.1.0"
  val arrowPostgresql   =   "org.postgresql"              %  "postgresql"       % "9.4-1204-jdbc42"
  val arrowSlickHikari  =   "com.typesafe.slick"          %% "slick-hikaricp"   % "3.1.0"
  val arrowApachHttp    =   "org.apache.httpcomponents"   %  "httpcore"         % "4.4.1"
  val arrowCommonsCodec =   "commons-codec"               % "commons-codec"     % "1.10"
  val arrowJodaTime     =   "joda-time"                   % "joda-time"         % "2.8.1"
  val arrowPlay         =   "com.typesafe.play"           %% "play-ws"          % "2.4.3"
  val arrowAnormCypher  =   "org.anormcypher"             %% "anormcypher"      % "0.7.1"
  val arrowJavaMail     =   "javax.mail"                  % "mail"              % "1.4.7"
  val arrowJavaActivation = "javax.activation" % "activation" % "1.1.1"
  //THESE ARE NOT NEEDED BECAUSE THE DEPS ARE IN THE PLAY-WS LIB
  //INCLUDING THEM CAUSES SLF4J COMPONENTS TO BE EVICTED IN PACKAGING AND BOMBS THE LOGGING
  //val arrowAkkaActor    =   "com.typesafe.akka"           %% "akka-actor"       % "2.4.0"
  //val arrowAkkaKernel   =   "com.typesafe.akka"           %% "akka-kernel"      % "2.4.0"
  //val arrowAkkaRemote   =   "com.typesafe.akka"           %% "akka-remote"      % "2.4.0"
  //val arrowAkkaSlf4j    =   "com.typesafe.akka"           %% "akka-slf4j"       % "2.4.0"

}

package com.exit25.arrow.productAd.http

import com.exit25.arrow.productAd.requests.AbstractRequest
import play.api.libs.ws.ning._
import play.api.libs.ws._
import scala.concurrent.Future


/**
 * Created by masinoa on 10/25/15.
 */
object HTTPService {
  val client = {
    /*
    DEFAULT HTTP CLIENT CONFIGURATION IN USE - SEE PLAY API DOCS
    https://www.playframework.com/documentation/2.4.x/api/scala/index.html#play.api.libs.ws.ning.NingWSClientConfig
    allowPoolingConnection: Boolean = true,
    allowSslConnectionPool: Boolean = true,
    ioThreadMultiplier: Int = 2,
    maxConnectionsPerHost: Int = 1,
    maxConnectionsTotal: Int = 1,
    maxConnectionLifetime: Duration = Duration.Inf,
    idleConnectionInPoolTimeout: Duration = 1.minute,
    webSocketIdleTimeout: Duration = 15.minutes,
    maxNumberOfRedirects: Int = 5,
    maxRequestRetry: Int = 5,
    disableUrlEncoding: Boolean = false
     */
    val builder = new NingAsyncHttpClientConfigBuilder()
    new NingWSClient((builder.build()))
  }
  def executeRequest(request:AbstractRequest): Future[WSResponse] = {

    request.REQUEST_METHOD match{
      case "GET" => client.url(request.requestURL()).get()
      case _ => throw new Exception("Unknown Request Type")
    }
  }
}

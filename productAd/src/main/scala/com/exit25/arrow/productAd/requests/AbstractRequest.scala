package com.exit25.arrow.productAd.requests


import java.net.URLEncoder

import com.exit25.arrow.productAd.auth.AWSKeyIdentifiers
import com.exit25.arrow.productAd.locales.Locale
import com.exit25.arrow.productAd.util.AWS4Signature
import com.exit25.arrow.productAd.util.Constants.UTF8_CHARSET

import scala.collection.SortedMap
import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTimeZone, DateTime}

/**
 * Created by masinoa on 10/25/15.
 */
abstract class AbstractRequest(queryParams : SortedMap[String,Any], credentials: AWSKeyIdentifiers, locale:Locale) extends AWS4Signature{
  val REQUEST_METHOD : String
  val OPERATION : String

  def required_aws_params() : Option[SortedMap[String,String]]

  lazy val default_required_aws_params = SortedMap(
    "AssociateTag" -> credentials.associateTag,
    "AWSAccessKeyId" -> credentials.accessKeyID,
    "Operation"->OPERATION,
    "Service" -> "AWSECommerceService",
    "Timestamp" -> new DateTime(DateTimeZone.UTC).toString(DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z")),
    "Version" -> "2013-08-01"
  )

  def requestURL() : String = {
    val all_params = required_aws_params() match{
      case Some(sm) => queryParams.++(sm.filterKeys(k=>{!queryParams.contains(k)}))
        .++(default_required_aws_params.filterKeys(k=>{!sm.contains(k) && !queryParams.contains(k)}))
      case _ => queryParams.++(default_required_aws_params.filterKeys(k=>{!queryParams.contains(k)}))
    }

    val query = queryString(all_params)
    val toSign = s"$REQUEST_METHOD\n${locale.endPoint}\n${locale.request_URI}\n$query"
    val sig = URLsignature(toSign, credentials)
    s"http://${locale.endPoint}${locale.request_URI}?$query&Signature=$sig"
  }

  private def queryString(params: SortedMap[String,Any]) : String = {
    if(params.isEmpty)""
    else (""/:params.keys)((s,k)=>
      s"$s&${percentEncode(k)}=${percentEncode(params(k).toString)}").drop(1)
  }

  private def percentEncode(s: String) : String = {
    URLEncoder.encode(s,UTF8_CHARSET).replace("+","%20").replace("*","%2A").replace("%7E","~")
  }

}

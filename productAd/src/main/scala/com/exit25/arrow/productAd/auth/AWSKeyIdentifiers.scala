package com.exit25.arrow.productAd.auth

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}

/**
 * Created by masinoa on 10/25/15.
 */
case class AWSKeyIdentifiers (accessKeyID:String, accessSecretKey:String, associateTag:String)

object AWSKeyIdentifiers{

  def credsFromConfig(configFile:File, path:String) : AWSKeyIdentifiers = {
    val config = ConfigFactory.parseFile(configFile)
    val credsConfig = config.getConfig(path)
    credsFromConfig(credsConfig)
  }

  def credsFromConfig(conf:Config) : AWSKeyIdentifiers = {
    val accessKeyId = conf.getString("accessKeyID")
    val secretAccessKey = conf.getString("secretAccessKey")
    val associateTag = conf.getString("associateTag")
    AWSKeyIdentifiers(accessKeyId, secretAccessKey,associateTag)
  }

}

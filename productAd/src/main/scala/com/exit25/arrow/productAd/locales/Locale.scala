package com.exit25.arrow.productAd.locales

/**
 * Created by masinoa on 10/25/15.
 */
case class Locale(id:Symbol, name:String, endPoint : String) {
  val endPoint_Secure : String = endPoint.replaceFirst("http","https")
  val request_URI : String = "/onca/xml"
}

object Locale{
  def apply(localId : Symbol) : Locale = {
    localId match{
      case 'US => Locale('US, "United States", "webservices.amazon.com")
      case 'UK => Locale('UK, "United Kingdom", "webservices.amazon.co.uk")
      case 'MX => Locale('MX, "Mexico","webservices.amazon.com.mx")
      case 'JP => Locale('JP, "Japan","webservices.amazon.co.jp")
      case 'IT => Locale('IT,"Italy","webservices.amazon.it")
      case 'IN => Locale('IN, "India","webservices.amazon.in")
      case 'FR => Locale('FR, "France","webservices.amazon.fr")
      case 'ES => Locale('ES,"Spain","webservices.amazon.es")
      case 'DE => Locale('DE, "Germany","webservices.amazon.de")
      case 'CN => Locale('CN, "China", "webservices.amazon.cn")
      case 'CA => Locale('CA, "Canada", "webservices.amazon.ca")
      case 'BR => Locale('BR, "Brazil","webservices.amazon.br")
      case _ => throw new Exception(s"Unknown Locale: $localId")
    }
  }
}

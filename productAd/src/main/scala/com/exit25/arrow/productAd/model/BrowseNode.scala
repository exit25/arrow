package com.exit25.arrow.productAd.model

import play.api.libs.ws.WSResponse

import scala.xml.{Node, Elem}
import com.exit25.arrow.productAd.util.XmlUtil.{findNode, getNodeValue}


/**
 * Created by masinoa on 11/7/15.
 */
case class BrowseNode(id: String, name:String, children: Option[List[BrowseNode]], ancestors: Option[List[BrowseNode]]){

}

object BrowseNode{

  def response2BrowseNode(response:WSResponse) :Option[BrowseNode] = {
    val body: Elem = scala.xml.XML.loadString(response.body)

    findNode("BrowseNodes", Left(body)) match{
      case Some(browseNodes: Node) =>{
         findNode("BrowseNode",Right(browseNodes)) match{
           case Some(browseNode) =>{
             getBrowseNodeId(browseNode) match{
               case Some(id) =>{
                 getBrowseNodeName(browseNode) match{
                   case Some(name) => {
                     val children = getChildren(browseNode)
                     val ancestors = getAncestors(browseNode)
                     Some(BrowseNode(id, name, children, ancestors))
                   }
                   case _ => None
                 }
               }
               case _ => None
             }
           }
           case _ => None
         }
      }
      case _ => None

    }

  }

  private def getChildren(node:Node) = getRelatives(node, "Children")

  private def getAncestors(node:Node) = getRelatives(node, "Ancestors")

  private def getRelatives(node:Node, label:String) : Option[List[BrowseNode]]= {
    findNode(label, Right(node)) match{
      case Some(n) => {
        if(n.child==null)None
        else{
          val rels = (List.empty[BrowseNode] /: n.child){(l,c) =>
            getBrowseNodeId(c) match{
              case Some(id: String) => {
                getBrowseNodeName(c) match {
                  case Some(name) => {BrowseNode(id,name,None,None)::l}
                  case _ => l
                }
              }
              case _ => l
            }
          }
          Some(rels)
        }
      }
      case _ => None
    }
  }

  private def getBrowseNodeId(node:Node) = getNodeValue(node, "BrowseNodeId")

  private def getBrowseNodeName(node:Node) = getNodeValue(node, "Name")

}

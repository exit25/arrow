package com.exit25.arrow.productAd.util

/**
 * Created by masinoa on 10/25/15.
 */
import java.net.URLEncoder
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import com.exit25.arrow.productAd.auth.AWSKeyIdentifiers
import org.apache.commons.codec.binary
import Constants.UTF8_CHARSET

trait AWSSignature{

  /**
   * generates an ASCII signature
   * @param stringToSign
   * @param credentials
   * @return
   */
  def signature(stringToSign:String, credentials:AWSKeyIdentifiers) : String

  /**
   * generates a URL Encoded signature using the specified codec
   * @param stringToSign
   * @param credentials
   * @param codec
   */
  def URLsignature(stringToSign: String, credentials:AWSKeyIdentifiers, codec:String = "UTF-8") : String = {
    val rawSignature = signature(stringToSign, credentials)
    URLEncoder.encode(rawSignature, codec)
  }

}

trait AWS4Signature extends AWSSignature{

  val HMAC_SHA256 = "HmacSHA256"

  /**
   * generates HMAC SHA 256 signature
   * @param stringToSign
   * @param credentials
   * @return
   */
  def signature(stringToSign:String, credentials:AWSKeyIdentifiers) : String = {
    val secretKeyBytes = credentials.accessSecretKey.getBytes(UTF8_CHARSET)
    val secretKeySpec = new SecretKeySpec(secretKeyBytes, HMAC_SHA256)
    val mac = Mac.getInstance(HMAC_SHA256)
    mac.init(secretKeySpec)
    val data = stringToSign.getBytes(UTF8_CHARSET)
    val rawHmac = mac.doFinal(data)
    val encoder = new binary.Base64()
    new String(encoder.encode(rawHmac))
  }

}

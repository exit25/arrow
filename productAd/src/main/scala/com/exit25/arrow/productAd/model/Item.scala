package com.exit25.arrow.productAd.model

import play.api.libs.ws.WSResponse
import com.exit25.arrow.productAd.util.XmlUtil.{findNode, getNodeValue, getNodeValuesFromPath, getNodeAttributeFromPath}
import scala.xml.Node

/**
 * Created by masinoa on 11/22/15.
 */
case class ItemElectronic(asin:String,
                          upc:String,
                          brand:String,
                          manufacturer:String,
                          title:String,
                          salesRank:Int,
                          binding:String,
                          detailPageUrl:String,
                          allOffersUrl:String,
                          browseNodeId:String,
                          browseNodeName:String,
                          weight:Int,
                          weightUnits:String,
                          length:Int,
                          height:Int,
                          width:Int,
                          lengthUnits:String)

case class ItemBook(asin:String,
                    title:String,
                    author:String,
                    publicationYear:Int,
                    salesRank:Int,
                    binding:String,
                    detailPageURL:String,
                    allOffersUrl:String,
                    edition:Int,
                    isbn:String,
                    publisher: String,
                    browseNodeId:String,
                    browseNodeName:String,
                    weight:Int,
                    weightUnits:String,
                    length:Int,
                    height:Int,
                    width:Int,
                    lengthUnits:String) {
}

object Item{
  val dims = List("ItemAttributes","ItemDimensions")

  def weightUnits(item:Node) = getNodeAttributeFromPath(item, List(("Weight","Units", dims))) match{
    case Some(m) => m.getOrElse("Weight:Units","UNK")
    case _ => "UNK"
  }

  def lengthUnits(item:Node) = getNodeAttributeFromPath(item, List(("Length","Units", dims))) match{
    case Some(m) =>m.getOrElse("Length:Units", "UNK")
    case _ => "UNK"
  }

  def allOffersURL(item:Node) = findNode("ItemLinks", Right(item)) match{
    case Some(itemLinks) => {
      if(itemLinks.child==null) None
      else{
        val aourl = ("" /: itemLinks.child){(url,c) =>
          getNodeValue(c, "Description") match {
            case Some(des) =>{
              if(des=="All Offers") getNodeValue(c,"URL") match{
                case Some(s) => s
                case _ => url
              }else url
            }
            case _ => url
          }
        }
        Some(aourl)
      }
    }
    case _ => None
  }
}

object ItemElectronic{

  def response2ItemElectronic(response:WSResponse) : Option[List[ItemElectronic]] = {
    response2ItemElectronic(response.body)
  }

  def response2ItemElectronic(responseBody:String) : Option[List[ItemElectronic]] = {
    val body = scala.xml.XML.loadString(responseBody)

    try {
      findNode("Items", Left(body)) match {
        case Some(itemsNode) => {
          if (itemsNode.child == null) None
          else {
            Some((List.empty[ItemElectronic] /: itemsNode.child)                                                                                                                                                                                                                                  { (l, item) =>
              if (item.label == "Item") {
                try{
                  buildItemElectronic(item) match {
                    case Some(ib) => ib :: l
                    case _ => l
                  }
                }catch {
                  case e:Exception =>{
                    //println(s"Error building book for ${item}")
                    l
                  }
                }
              }
              else l
            })
          }
        }
        case _ => None
      }
    }catch{
      case e:Exception => {
        //println(responseBody)
        println(s"ERROR: ${e.getMessage}, reconstructing book")
        None
      }
    }
  } //end response2ItemElectronic

  private def buildItemElectronic(item:Node) : Option[ItemElectronic] = {
    val la = List("ItemAttributes")
    val e = List.empty[String]
    val dims = Item.dims

    val params = getNodeValuesFromPath(item, List(
      ("ASIN","ASIN", e),
      ("UPC","UPC", la),
      ("Brand","Brand", la),
      ("Manufacturer","Manufacturer",la),
      ("Title","Title", la),
      ("SalesRank","SalesRank", e),
      ("Binding","Binding", la),
      ("DetailPageURL","DetailPageURL", e),
      ("BrowseNodeId","BrowseNodeId", List("BrowseNodes","BrowseNode")),
      ("BrowseNodeName","Name", List("BrowseNodes","BrowseNode")),
      ("Weight","Weight", dims),
      ("Height","Height", dims),
      ("Length","Length", dims),
      ("Width","Width", dims)
    ))

    val weightUnits: String = Item.weightUnits(item)

    val lengthUnits = Item.lengthUnits(item)

    val allOffersURL = Item.allOffersURL(item)

    Item.allOffersURL(item) match{
      case Some(aourl) => {
        params match{
          case Some(m) =>{
            m.get("ASIN") match{
              case Some(asin) =>{
                if(asin.trim.length>0){
                  Some(ItemElectronic(
                    asin,
                    m.getOrElse("UPC",""),
                    m.getOrElse("Brand",""),
                    m.getOrElse("Manufacturer",""),
                    m.getOrElse("Title",""),
                    m.getOrElse("SalesRank","-1").toInt,
                    m.getOrElse("Binding",""),
                    m.getOrElse("DetailPageURL",""),
                    aourl,
                    m.getOrElse("BrowseNodeId",""),
                    m.getOrElse("BrowseNodeName",""),
                    m.getOrElse("Weight","0").toInt,
                    weightUnits,
                    m.getOrElse("Length","0").toInt,
                    m.getOrElse("Height","0").toInt,
                    m.getOrElse("Width","0").toInt,
                    lengthUnits
                  ))
                }else None
              }
              case _ => None
            }

          }
          case _ => None
        }
      }
      case _ => None
    }
  }


}

object ItemBook{

  def response2ItemBooks(response:WSResponse) : Option[List[ItemBook]] = {
    response2ItemBooks(response.body)
  }

  def response2ItemBooks(responseBody:String) : Option[List[ItemBook]] = {
    val body = scala.xml.XML.loadString(responseBody)

    try {
      findNode("Items", Left(body)) match {
        case Some(itemsNode) => {
          if (itemsNode.child == null) None
          else {
            Some((List.empty[ItemBook] /: itemsNode.child)                                                                                                                                                                                                                                  { (l, item) =>
              if (item.label == "Item") {
                try{
                  buildItemBook(item) match {
                    case Some(ib) => ib :: l
                    case _ => l
                  }
                }catch {
                  case e:Exception =>{
                    //println(s"Error building book for ${item}")
                    l
                  }
                }
              }
              else l
            })
          }
        }
        case _ => None
      }
    }catch{
      case e:Exception => {
        //println(responseBody)
        println(s"ERROR: ${e.getMessage}, reconstructing book")
        None
      }
    }
  }

  private def buildItemBook(item:Node) : Option[ItemBook] = {
    val la = List("ItemAttributes")
    val e = List.empty[String]
    val dims = Item.dims
    val params =  getNodeValuesFromPath(item, List(
        ("ASIN","ASIN", e),
        ("DetailPageURL","DetailPageURL", e),
        ("SalesRank","SalesRank", e),
        ("Author","Author", la),
        ("Binding","Binding", la),
        ("Title","Title", la),
        ("PublicationDate","PublicationDate", la),
        ("Edition","Edition", la),
        ("ISBN","ISBN", la),
        ("Publisher","Publisher", la),
        ("BrowseNodeId","BrowseNodeId", List("BrowseNodes","BrowseNode")),
        ("BrowseNodeName","Name", List("BrowseNodes","BrowseNode")),
        ("Weight","Weight", dims),
        ("Height","Height", dims),
        ("Length","Length", dims),
        ("Width","Width", dims)
    ))

    val weightDims: String = Item.weightUnits(item)

    val lengthDims = Item.lengthUnits(item)

    val ordinalsMap = Map("first"->1, "second"->2, "third"->3, "fourth"->4,
      "fifth"->5, "sixth"->6, "seventh"->7, "eighth"->8,
      "ninth"->9, "tenth"->10, "eleventh"->11, "tweleth"->12)
    val oridinals = ordinalsMap.keys.toList

    Item.allOffersURL(item) match {
      case Some(aourl) => {
        params match {
          case Some(m) => {
            val pubDate = m.getOrElse("PublicationDate","0").split("-")(0).toInt
            val numbers = Range(0,10).map{n =>
              val ca = n.toString.toCharArray
              ca(0)
            }
            val ed = m.getOrElse("Edition","").toLowerCase
            val ordKey = (""/:oridinals){(s1,s2) =>
              if(ed.contains(s2))s2 else s1
            }
            val edition: Int = if(oridinals.contains(ordKey))ordinalsMap.getOrElse(ordKey,0)
             else{
                val editionString = (""/:ed){(s,n) =>
                  if(numbers.contains(n)) s+n
                  else s
                }
                if(editionString.length>0)editionString.toInt
                else 0
              }
            m.get("ASIN") match{
              case Some(asin) => {
                if(asin.trim.length>0){
                  Some(ItemBook(asin,
                    m.getOrElse("Title",""),
                    m.getOrElse("Author",""),
                    pubDate,
                    m.getOrElse("SalesRank","-1").toInt,
                    m.getOrElse("Binding",""),
                    m.getOrElse("DetailPageURL",""),
                    aourl,
                    edition,
                    m.getOrElse("ISBN",""),
                    m.getOrElse("Publisher",""),
                    m.getOrElse("BrowseNodeId",""),
                    m.getOrElse("BrowseNodeName",""),
                    m.getOrElse("Weight","0").toInt,
                    weightDims,
                    m.getOrElse("Length","0").toInt,
                    m.getOrElse("Height","0")toInt,
                    m.getOrElse("Width","0").toInt,
                    lengthDims
                  ))
                }else None
              }
              case _ => None
            }
          }
          case _ => None
        }
      }
      case _ => None
    }

  }

}

case class ItemRequestInfo(totalResults:Int, itemPage:Int, totalPages:Int, keywords:Option[String], browseNodeId:Option[String])

object ItemRequestInfo{

  def response2ItemRequestInfo(response:WSResponse) : Option[ItemRequestInfo] = {
   response2ItemRequestInfo(response.body)
  }

  def response2ItemRequestInfo(responseBody:String) : Option[ItemRequestInfo] = {
    val body = scala.xml.XML.loadString(responseBody)

    try {
      findNode("Items", Left(body)) match {
        case Some(items) => {
          val rslt: Option[ItemRequestInfo] = for {
            totalResults <- getNodeValue(items, "TotalResults")
            totalPages <- getNodeValue(items, "TotalPages")
            isr <- findNode(List("Request", "ItemSearchRequest"), Right(items))
            itemPage <- getNodeValue(isr, "ItemPage")
          } yield {
              val kw = getNodeValue(isr, "Keywords")
              val bn = getNodeValue(isr, "BrowseNode")
              ItemRequestInfo(totalResults.toInt, itemPage.toInt, totalPages.toInt, kw, bn)
            }
          rslt
        }
        case _ => None
      }
    } catch{
      case e:Exception => {
        //println(responseBody)
        //println(s"ERROR: ${e.getMessage}, reconstructing request info")
        None
      }
    }
  }


}
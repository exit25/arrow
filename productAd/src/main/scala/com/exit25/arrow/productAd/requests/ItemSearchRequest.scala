package com.exit25.arrow.productAd.requests

import com.exit25.arrow.productAd.auth.AWSKeyIdentifiers
import com.exit25.arrow.productAd.http.HTTPService
import com.exit25.arrow.productAd.indicies.SearchIndex
import com.exit25.arrow.productAd.locales.Locale
import play.api.libs.ws.WSResponse

import scala.collection.SortedMap
import scala.concurrent.Future

/**
 * Created by masinoa on 10/25/15.
 */
case class ItemSearchRequest (queryParams : SortedMap[String,Any], credentials: AWSKeyIdentifiers,
                              searchIndex : SearchIndex, locale: Locale)
  extends AbstractRequest(queryParams , credentials, locale){

  val REQUEST_METHOD = "GET"
  val OPERATION = "ItemSearch"


  def required_aws_params() : Option[SortedMap[String,String]] = Some(SortedMap("SearchIndex"->searchIndex.name))
}

object ItemSearchRequest{

  def execute(request:ItemSearchRequest) : Future[WSResponse] = {
    HTTPService.executeRequest(request)
  }

  def apply(keywords: String, itemPage:Int, credentials: AWSKeyIdentifiers, searchIndex: SearchIndex, locale: Locale)
  : ItemSearchRequest = {
    val queryParams = SortedMap(
      "Keywords"->keywords,
      "ItemPage"->itemPage.toString,
      "ResponseGroup"->"ItemAttributes, SalesRank, BrowseNodes"
    )
    ItemSearchRequest(queryParams, credentials, searchIndex, locale)
  }

  def apply(keywords: String, browseNodeID: String, itemPage:Int,
            credentials: AWSKeyIdentifiers, searchIndex: SearchIndex, locale: Locale) : ItemSearchRequest = {
    val queryParams = SortedMap(
      "Keywords"->keywords,
      "BrowseNode"->browseNodeID,
      "ItemPage"->itemPage.toString,
      "ResponseGroup"->"ItemAttributes, SalesRank, BrowseNodes"
    )
    ItemSearchRequest(queryParams, credentials, searchIndex, locale)
  }


}

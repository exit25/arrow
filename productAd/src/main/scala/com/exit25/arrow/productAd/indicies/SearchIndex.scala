package com.exit25.arrow.productAd.indicies

import com.exit25.arrow.productAd.locales.Locale

/**
 * Created by masinoa on 10/25/15.
 */
abstract class SearchIndex(locale:Locale) {
  val name : String
  val rootBrowseNode: Int
  val queryOptions: List[QueryOption]
  val sortOptions: List[SortOption]
}



case class Books(locale:Locale) extends SearchIndex(locale){
  val name = Books.name

  val rootBrowseNode = locale.id match {
    case 'US => 172282
    case _ => 172282
  }

  val queryOptions = List(QueryOption('Author,"Author", "The Book Author"))

  val sortOptions = List(SortOption("NOT DONE YET"))

}

object Books{
  val name = "Books"
  val queryOptions = List(QueryOption('Author,"Author", "The Book Author"))
  val sortOptions = List(SortOption("NOT DONE YET"))
}

case class Electronics(locale:Locale) extends SearchIndex(locale){
  val name = Electronics.name

  val rootBrowseNode = locale.id match{
    case 'US => 493964
    case _ => 493964
  }

  val queryOptions = List.empty[QueryOption]

  val sortOptions = List.empty[SortOption]
}

object Electronics{
  val name = "Electronics"
}

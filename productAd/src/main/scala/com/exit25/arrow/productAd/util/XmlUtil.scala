package com.exit25.arrow.productAd.util

import scala.annotation.tailrec
import scala.xml.{NodeSeq, Node, Elem}

/**
 * Created by masinoa on 11/22/15.
 */
object XmlUtil {

  def findNode(label:String, root:Either[Elem,Node]) : Option[Node] = {
    val start : Option[Node] = None
    root match{
      case Left(elem) => {
        if(elem.child==null) None
        else{
          (start/:elem.child){(o,c) =>
            if(c.label==label)Some(c)
            else o
          }
        }
      }
      case Right(node) =>{
        if(node.child==null) None
        else{
          (start/:node.child){(o,c) =>
            if(c.label==label)Some(c)
            else o
          }
        }
      }
    }
  }

  @tailrec
  def findNode(labelPath:List[String], root:Either[Elem,Node], foundNode:Option[Node]=None) : Option[Node] = {
    if(labelPath.isEmpty)foundNode
    else{
      findNode(labelPath.head, root) match{
        case Some(n) => findNode(labelPath.tail, Right(n), Some(n))
        case _ => None
      }
    }
  }

  def getNodeAttribute(node:Node, nodeLabel:String, attributeLabel:String) = {
    findNode(nodeLabel, Right(node)) match{
      case Some(v) => {
        val ns = v \\ s"@$attributeLabel"
        Some(ns.head.text)
      }
      case _ => None
    }
  }

  @tailrec
  def getNodeAttributeFromPath(item:Node, paths:List[(String,String,List[String])], nodeAttributes:Option[Map[String,String]]=None)
  : Option[Map[String,String]]= {
    if(paths.isEmpty) nodeAttributes
    else{
      val (nodeLabel,attributeLabel, path) = paths.head
      if(path.isEmpty){
        getNodeAttribute(item, nodeLabel, attributeLabel) match {
          case Some(v) => nodeAttributes match {
            case Some(m) => getNodeAttributeFromPath(item, paths.tail, Some(m.+(s"$nodeLabel:$attributeLabel"->v)))
            case _ => getNodeAttributeFromPath(item, paths.tail, Some(Map(s"$nodeLabel:$attributeLabel"->v)))
          }
          case _ =>getNodeAttributeFromPath(item, paths.tail, nodeAttributes)
        }
      }
      else{
        findNode(path, Right(item)) match{
          case Some(n) => {
            getNodeAttribute(n, nodeLabel, attributeLabel) match {
              case Some(v) => nodeAttributes match{
                case Some(m) => getNodeAttributeFromPath(item, paths.tail, Some(m.+(s"$nodeLabel:$attributeLabel"->v)))
                case _ => getNodeAttributeFromPath(item, paths.tail, Some(Map(s"$nodeLabel:$attributeLabel"->v)))
              }
              case _ => getNodeAttributeFromPath(item, paths.tail, nodeAttributes)
            }
          }
          case _ => getNodeAttributeFromPath(item, paths.tail, nodeAttributes)
        }
      }
    }
  }

  def getNodeValue(node:Node, label:String) = {
    findNode(label, Right(node)) match {
      case Some(v) => Some(v.text)
      case _ => None
    }
  }

  @tailrec
  def getNodeValuesFromPath(item:Node, paths:List[(String,String,List[String])], nodeValues:Option[Map[String,String]]=None)
  : Option[Map[String,String]]= {
    if(paths.isEmpty) nodeValues
    else{
      val (key,awsKey,path) = paths.head
      if(path.isEmpty){
        getNodeValue(item, awsKey) match {
          case Some(v) => nodeValues match {
            case Some(m) => getNodeValuesFromPath(item, paths.tail, Some(m.+(key->v)))
            case _ => getNodeValuesFromPath(item, paths.tail, Some(Map(key->v)))
          }
          case _ =>getNodeValuesFromPath(item, paths.tail, nodeValues)
        }
      }
      else{
        findNode(path, Right(item)) match{
          case Some(n) => {
            getNodeValue(n, awsKey) match {
              case Some(v) => nodeValues match{
                case Some(m) => getNodeValuesFromPath(item, paths.tail, Some(m.+(key->v)))
                case _ => getNodeValuesFromPath(item, paths.tail, Some(Map(key->v)))
              }
              case _ => getNodeValuesFromPath(item, paths.tail, nodeValues)
            }
          }
          case _ => getNodeValuesFromPath(item, paths.tail, nodeValues)
        }
      }
    }
  }

}

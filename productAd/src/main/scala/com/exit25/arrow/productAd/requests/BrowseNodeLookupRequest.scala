package com.exit25.arrow.productAd.requests

import com.exit25.arrow.productAd.auth.AWSKeyIdentifiers
import com.exit25.arrow.productAd.http.HTTPService
import com.exit25.arrow.productAd.locales.Locale
import play.api.libs.ws.WSResponse

import scala.collection.SortedMap
import scala.concurrent.Future
//import play.api.libs.concurrent.Execution.Implicits._

/**
 * Created by masinoa on 11/1/15.
 */
case class BrowseNodeLookupRequest (browseNodeId:String, credentials:AWSKeyIdentifiers,
                                locale: Locale)
  extends AbstractRequest(SortedMap("BrowseNodeId"->browseNodeId), credentials, locale){

  val REQUEST_METHOD = "GET"

  val OPERATION = "BrowseNodeLookup"

  def required_aws_params() : Option[SortedMap[String,String]] = None
}

object BrowseNodeLookupRequest{

  def execute(request:BrowseNodeLookupRequest): Future[WSResponse] = {
    HTTPService.executeRequest(request)
  }

}
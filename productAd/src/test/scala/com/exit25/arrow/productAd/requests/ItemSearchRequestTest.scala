package com.exit25.arrow.productAd.requests

import java.io.File

import com.exit25.arrow.productAd.auth.AWSKeyIdentifiers
import com.exit25.arrow.productAd.indicies.{Electronics, Books}
import com.exit25.arrow.productAd.locales.Locale
import com.exit25.arrow.productAd.model.{ItemElectronic, ItemBook, ItemRequestInfo}
import org.scalatest.{ShouldMatchers, FlatSpec}
import play.api.libs.ws.WSResponse

import scala.collection.SortedMap
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future}
import play.api.libs.concurrent.Execution.Implicits._

/**
 * Created by masinoa on 11/22/15.
 */
class ItemSearchRequestTest extends FlatSpec with ShouldMatchers{

  val creds = AWSKeyIdentifiers.credsFromConfig(new File("./conf/application.conf"), "arrow.productAd.credentials")
  //val ip = 1
  val kw = "Calculus Stewart"

  val queryParams = SortedMap(
    "BrowseNode"->"491306",
    "ItemPage"->"1",
    "ResponseGroup"->"ItemAttributes, SalesRank, BrowseNodes",
    //"Sort"->"salesrank"
    //"Sort"->"daterank"
    "Sort"->"relevancerank"
  )

  val request = ItemSearchRequest(queryParams, creds, Books(Locale('US)), Locale('US))


  "A valid book search index ItemSearchRequest" should "have a response" in {

   // val request = ItemSearchRequest(kw, ip, creds, Books(Locale('US)), Locale('US))

    try{
      val response : Future[WSResponse] = ItemSearchRequest.execute(request)

      val itemSearchResult = response map { r =>
        val body = r.body
        //println(body)
        ItemRequestInfo.response2ItemRequestInfo(body) match {
          case Some(info) => {
            info.itemPage should equal(1)
            //info.keywords should equal()
            info.totalPages should be > 1
            info.totalResults should be > 1
          }
          case _ => fail("Conversion from response to ItemRequest Info failed")
        }

        ItemBook.response2ItemBooks(body) match {
          case Some(itemBooks) =>{
            itemBooks.length should be > 0
            itemBooks.foreach{_.asin.length should be > 0}
            itemBooks.foreach{b => println(s"${b.asin},${b.publicationYear}, ${b.browseNodeName} ${b.salesRank} ${b.length} X ${b.height} X ${b.width} ${b.lengthUnits}, ${b.weight} ${b.weightUnits}")}
          }
          case _ => fail("Conversion from response to ItemBook failed")
        }


      }
      Await.result(itemSearchResult, DurationInt(20).seconds)
    }catch{
      case e:Exception => fail(s"test failed --- ${e.getMessage}")
    }

  }

  val queryParams2 = SortedMap(
    "Keywords"->"Apple IPad",
    "ItemPage"->"1",
    "ResponseGroup"->"ItemAttributes, SalesRank, BrowseNodes",
    "Manufacturer"->"Apple"
    //"Sort"->"salesrank"
    //"Sort"->"daterank"
    //"Sort"->"relevancerank"
  )

  //val request = ItemSearchRequest(queryParams, creds, Books(Locale('US)), Locale('US))

  val request2 = ItemSearchRequest(queryParams2, creds, Electronics(Locale('US)), Locale('US))

  "A valid electronics search index ItemSearchRequest" should "have a response" in {
    try{
      val response : Future[WSResponse] = ItemSearchRequest.execute(request2)

      val itemSearchResult = response map { r =>
        val body = r.body
        println(body)
        ItemRequestInfo.response2ItemRequestInfo(body) match {
          case Some(info) => {
            info.itemPage should equal(1)
            //info.keywords should equal()
            info.totalPages should be > 1
            info.totalResults should be > 1
          }
          case _ => fail("Conversion from response to ItemRequest Info failed")
        }

        ItemElectronic.response2ItemElectronic(body) match {
          case Some(items) =>{
            items.length should be > 0
            items.foreach{_.asin.length should be > 0}
            items.foreach{b => println(s"--xx--${b.asin},${b.upc}, ${b.browseNodeName},${b.brand},${b.salesRank} ${b.length} X ${b.height} X ${b.width} ${b.lengthUnits}, ${b.weight} ${b.weightUnits}")}
          }
          case _ => fail("Conversion from response to ItemElectronic failed")
        }


      }
      Await.result(itemSearchResult, DurationInt(20).seconds)
    }catch{
      case e:Exception => fail(s"test failed --- ${e.getMessage}")
    }
  }

}

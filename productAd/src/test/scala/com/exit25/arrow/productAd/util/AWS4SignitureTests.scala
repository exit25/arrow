package com.exit25.arrow.productAd.util

/**
 * Created by masinoa on 10/31/15.
 */

import com.exit25.arrow.productAd.auth.AWSKeyIdentifiers
import org.scalatest._

class AWS4SignitureTests extends FunSpec with GivenWhenThen with ShouldMatchers with BeforeAndAfter with AWS4Signature{
  val creds = AWSKeyIdentifiers("xyz", "1234567890", "notag")
  describe("AWS4Signiture"){
    it("should generate a signature key for requests"){
      Given("a properly encoded string to sign")

      //build the request string manually for this test
      val req_method = "GET"
      val endpoint = "webservices.amazon.com"
      val req_uri = "/onca/xml"
      val canonicalQS = s"AWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&AssociateTag=mytag-20&ItemId=0679722769&Operation=ItemLookup&ResponseGroup=Images%2CItemAttributes%2COffers%2CReviews&Service=AWSECommerceService&Timestamp=2014-08-18T12%3A00%3A00Z&Version=2013-08-01"
      val toSign = s"$req_method\n$endpoint\n$req_uri\n$canonicalQS"

      val rawSignature = signature(toSign, creds)
      rawSignature should equal("j7bZM0LXZ9eXeZruTqWm2DIvDYVUU3wxPPpp+iXxzQc=")

      val urlSignature = URLsignature(toSign, creds)
      urlSignature should equal("j7bZM0LXZ9eXeZruTqWm2DIvDYVUU3wxPPpp%2BiXxzQc%3D")
    }
  }
}

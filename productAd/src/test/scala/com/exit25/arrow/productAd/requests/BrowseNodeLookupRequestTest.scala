package com.exit25.arrow.productAd.requests

import java.io.File

import com.exit25.arrow.productAd.auth.AWSKeyIdentifiers
import com.exit25.arrow.productAd.locales.Locale
import com.exit25.arrow.productAd.model.BrowseNode
import org.scalatest.{ShouldMatchers, FlatSpec}
import play.api.libs.ws.WSResponse
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future}
import play.api.libs.concurrent.Execution.Implicits._

/**
 * Created by masinoa on 11/1/15.
 */
class BrowseNodeLookupRequestTest extends FlatSpec with ShouldMatchers{

  //Books browse node id should always be valid
  val browseNodeId = "468214"
  val creds = AWSKeyIdentifiers.credsFromConfig(new File("./conf/application.conf"), "arrow.productAd.credentials")

  "A valid BrowseNodeLookupRequest" should "have a response" in {
    val request = BrowseNodeLookupRequest(browseNodeId, creds, Locale('US))
    try{
      val responses : Future[WSResponse] = BrowseNodeLookupRequest.execute(request)

      val browseNodes = responses map{ r =>
        BrowseNode.response2BrowseNode(r) match {
          case Some(subjects) =>{
            subjects.id should equal(browseNodeId)
            subjects.name should equal("Subjects")
            subjects.children match{
              case Some(children) => children.length should be > 0
              case _ => fail("subjects should have children")
            }
            subjects.ancestors match{
              case Some(ancestors) =>{
                ancestors.head.name should equal("Books")
              }
              case _ => fail("subjects should have the books ancestor")
            }
          }
          case _ => fail("Conversion from response to BrowseNode failed")
        }
      }

      browseNodes.onFailure{
        case t => fail(s"Request failed with message --- ${t.getMessage}")
      }

      Await.result(browseNodes, DurationInt(20).seconds)
    }catch{
      case e:Exception => fail(s"Request failed --- ${e.getMessage}")
    }
  }

}

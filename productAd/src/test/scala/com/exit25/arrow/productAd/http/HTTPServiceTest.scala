package com.exit25.arrow.productAd.http

/**
 * Created by masinoa on 10/31/15.
 */

import java.io.File

import com.exit25.arrow.productAd.auth.AWSKeyIdentifiers
import com.exit25.arrow.productAd.indicies.Books
import com.exit25.arrow.productAd.locales.Locale
import com.exit25.arrow.productAd.requests.ItemSearchRequest
import org.scalatest._
import play.api.libs.ws.WSResponse
import play.api.libs.concurrent.Execution.Implicits._
import scala.concurrent.{Future, Await}
import scala.concurrent.duration.DurationInt

import scala.collection.SortedMap

/**
 * Created by masinoa on 9/6/15.
 */
class HTTPServiceTest extends FunSpec with GivenWhenThen with ShouldMatchers with BeforeAndAfter{
  val creds = AWSKeyIdentifiers.credsFromConfig(new File("./conf/application.conf"), "arrow.productAd.credentials")

  describe("The HTTPService"){
    it("should execute valid HTTP requests"){
      Given("a Request object")
      val params = SortedMap("AssociateTag"->"mytag-20",
        "ResponseGroup"->"ItemAttributes,BrowseNodes",
        "Keywords"->"Calculus",
        "Author"->"Stewart")

      val request = ItemSearchRequest(params, creds, Books(Locale('US)), Locale('US))
      try{
        val responses: Future[WSResponse] = HTTPService.executeRequest(request)
        responses.onSuccess{
          case r:WSResponse =>{
            r.body.length should be >0
          }
        }
        Await.result(responses, DurationInt(20).second)
      }catch{
        case e:Exception => fail("Request failed")
      }
    }
  }
}

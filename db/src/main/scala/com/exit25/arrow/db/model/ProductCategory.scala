package com.exit25.arrow.db.model

/**
  * Created by masinoa on 9/1/16.
  */
abstract class ProductCategory {

  val baseName : String

}

object ElectronicsCategory extends ProductCategory{

  val baseName = "ELECTRONICS"

}

object BooksCategory extends ProductCategory{

  val baseName = "BOOKS"

}


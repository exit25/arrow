package com.exit25.arrow.db.apps

import java.io.File
import java.sql.Date

import scala.compat.Platform

//import akka.event.Logging
import com.exit25.arrow.db.model._
import com.typesafe.config.ConfigFactory
import slick.driver.PostgresDriver
import slick.driver.PostgresDriver.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
 * Created by masinoa on 10/17/15.
 */
object DBBuilder {

  def run() : Unit = {

    val config = ConfigFactory.parseFile(new File("./conf/application.conf"))
    val db: PostgresDriver.backend.DatabaseDef = Database.forConfig("arrow.db", config)
    //val logger = LoggerFactory.getLogger("com.exit25.arrow.db.DBBuilder")
    //val logger = Logging(context.system, this)

    val now = Platform.currentTime
    val today = new Date(now)

    try {
      val books = TableQuery[Books]
      val electronics = TableQuery[Electronics]

      //val lol = LowestOfferListingsTableQuery(today)

      val psq = TableQuery[ProductSearchesQueries]

     // val cp = CompetitivePricesTableQuery(today)

      val setupAction = DBIO.seq(
        //the lol, cp, and sales rank tables are now built on the fly
        //(books.schema ++ electronics.schema ++ lol.schema ++ cp.schema ++ psq.schema).create
        (books.schema ++ electronics.schema ++ psq.schema).create
      )

      //logger.info("Initiating BOOKS, ELECTRONICS, COMPETITIVE_PRICES, LOWEST_OFFER_LISTINGS, PRODUCT_SEARCH_QUERIES table creation")
      val setupFuture = db.run(setupAction)

      Await.result(setupFuture, Duration.Inf)

      //logger.info("Table creation completed")
      println("TABLE CREATION COMPLETE")

    } finally db.close

  }
}

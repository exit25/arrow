package com.exit25.arrow.db.model

import java.sql.Timestamp
import slick.driver.PostgresDriver.api._

/**
 * Created by masinoa on 10/25/15.
 */

case class ProductSearchQuery(id:Option[Int],
                         searchIndex:String,
                         keywords:Option[String],
                         browseNodeID:Option[String],
                         searchedOn:Timestamp)

class ProductSearchesQueries(tag:Tag)  extends Table[ProductSearchQuery](tag,"PRODUCT_SEARCH_QUERIES"){
  def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
  def searchIndex = column[String]("SEARCH_INDEX")
  def keywords = column[Option[String]]("KEYWORDS")
  def browseNodeID = column[Option[String]]("BROWSE_NODE_ID")
  def searchedOn = column[Timestamp]("SEARCHED_ON")

  def idx =index("IDX_BROWSE_NODE_ID", browseNodeID)

  def * = (id.?, searchIndex, keywords, browseNodeID, searchedOn) <> (ProductSearchQuery.tupled, ProductSearchQuery.unapply)
}

package com.exit25.arrow.db.model

import java.sql.Date

import slick.driver.PostgresDriver.api._
import slick.jdbc.meta.MTable
import slick.lifted.TableQuery

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by masinoa on 6/7/16.
  */
case class SalesRank (id:Option[Int],
                      asin: String,
                      salesRank: Int,
                      createdOn : Date)

class SalesRanks(tag: Tag, tableName: String = "SALES_RANKS")
  extends Table[SalesRank](tag, tableName){

  def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
  def asin = column[String]("ASIN")
  def salesRank = column[Int]("SALES_RANK")
  def createdOn = column[Date]("CREATED_ON")
  def idx = index("IDX_ASIN", (asin))

  def * = (id.?, asin, salesRank, createdOn) <> (SalesRank.tupled, SalesRank.unapply)
}

object SalesRanksTableQuery{

  def apply(tableName:String): TableQuery[SalesRanks] = {
    TableQuery[SalesRanks]((tag:Tag) => new SalesRanks(tag, tableName))
  }

  def apply(date: Date, pc:ProductCategory): TableQuery[SalesRanks] = {
    apply(tableName(date, pc))
  }

  def tableName(date:Date, pc: ProductCategory) = {
    val dateSuffix = yearMonthSuffix(date)
    s"SALES_RANKS_${pc.baseName}_$dateSuffix"
  }

  def yearMonthSuffix(date:Date) = {
    val Array(yyyy,mm,dd) = date.toString.split('-')
    s"$yyyy$mm"
  }

  def createIfNotExists(pc:ProductCategory, date:Date, db:Database)(implicit ec: ExecutionContext): Future[Vector[MTable]] = {
    db.run(MTable.getTables).andThen{case s =>
      s.map{tables =>
        val name = tableName(date, pc)
        val tableQry: TableQuery[SalesRanks] = apply(name)
        if(!tables.toList.contains(name)){
          db.run(tableQry.schema.create)
        }
      }
    }
  }

}

package com.exit25.arrow.db.apps

import java.io.File
import java.util.Properties
import javax.activation.{DataHandler, FileDataSource}
import javax.mail.internet.{InternetAddress, MimeBodyPart, MimeMessage, MimeMultipart}
import javax.mail.{Message, Session, Transport}

import com.typesafe.config.ConfigFactory

/**
 * Created by masinoa on 4/10/16.
 */
object Email {
  private val appconfig = ConfigFactory.parseFile(new File("./conf/application.conf"))
  private val analysisConfig = appconfig.getConfig("arrow.analysis")
  private val config = appconfig.getConfig("arrow.analysis.notifications")
  private val senderConfig = config.getConfig("sender")
  val recipients = config.getStringList("recipients")
  val user = senderConfig.getString("username")
  val pw = senderConfig.getString("password")
  val smtp = senderConfig.getString("smtp")
  val port = senderConfig.getString("port")
  val auth = senderConfig.getString("auth")
  val starttlsEnable = senderConfig.getString("starttlsEnable")

  val props = new Properties()
  props.put("mail.smtp.auth", auth);
  props.put("mail.smtp.starttls.enable", starttlsEnable);
  props.put("mail.smtp.host", smtp);
  props.put("mail.smtp.port", port);

  def sendEmail(subject: String, msg: String, attachements : Option[List[String]]) = {

    val authenticator = new SimplePWAuthenticator(user, pw)
    val session = Session.getInstance(props, authenticator)
    try{
      val message = new MimeMessage(session)
      message.setFrom(new InternetAddress(user))
      recipients.toArray.foreach{r => message.addRecipient(Message.RecipientType.TO,
        new InternetAddress(r.toString))}
      message.setSubject(subject)

      //create text body part
      val messageBP1 = new MimeBodyPart()
      messageBP1.setText(msg)

      val multipart = new MimeMultipart()

      multipart.addBodyPart(messageBP1)

      //add attachments
      attachements match{
        case Some(l) => l foreach{ a =>
          val mbp = new MimeBodyPart()
          val source = new FileDataSource(a)
          mbp.setDataHandler(new DataHandler(source))
          mbp.setFileName(a)
          multipart.addBodyPart(mbp)
        }
        case _ => {}
      }

      message.setContent(multipart)
      Transport.send(message)
    } catch {
      case e:Exception => println(e)
    }
  }
}

class SimplePWAuthenticator(user:String, pw:String) extends javax.mail.Authenticator{
  override protected def getPasswordAuthentication() ={
    new javax.mail.PasswordAuthentication(user,pw)
  }
}
package com.exit25.arrow.db.model

/**
 * Created by masinoa on 10/18/15.
 */

import java.sql.Date
import java.util.Calendar

import slick.driver.PostgresDriver.api._
import slick.jdbc.meta.MTable
import slick.lifted.TableQuery

import scala.concurrent.{ExecutionContext, Future}

case class LowestOfferListing( id : Option[Int],
                               asin : String,
                               landedPrice: BigDecimal,
                               listPrice: BigDecimal,
                               shipping: BigDecimal,
                               fulfillment: String,
                               condition: String,
                               subCondition: Option[String],
                               sellerFeedback: Option[String],
                               multipleOffersAtLowestPrice: Option[Boolean],
                               createdOn : Date)

class LowestOfferListings(tag: Tag, tableName: String = "LOWEST_OFFER_LISTINGS")
  extends Table[LowestOfferListing](tag, tableName){

  def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
  def asin = column[String]("ASIN")
  def landedPrice = column[BigDecimal]("LANDED_PRICE")
  def listPrice = column[BigDecimal]("LIST_PRICE")
  def shipping = column[BigDecimal]("SHIPPING")
  def fulfillment = column[String]("FULFILLMENT")
  def condition = column[String]("CONDITION")
  def subCondition = column[Option[String]]("SUB_CONDITION")
  def sellerFeedback = column[Option[String]]("SELLER_FEEDBACK")
  def multipleOffersAtLowestPrice = column[Option[Boolean]]("MULTIPLE_OFFERS_AT_LOWEST_PRICE")
  def idx = index("IDX_ASIN", asin)
  def createdOn = column[Date]("CREATED_ON")


  def * = (id.?, asin, landedPrice, listPrice, shipping, fulfillment,
    condition, subCondition, sellerFeedback, multipleOffersAtLowestPrice,
    createdOn) <> (LowestOfferListing.tupled, LowestOfferListing.unapply)
}

object LowestOfferListingsTableQuery{

  def apply(tableName:String) : TableQuery[LowestOfferListings] = {
    TableQuery[LowestOfferListings]((tag:Tag) => new LowestOfferListings(tag, tableName))
  }

  def apply(date:Date, pc: ProductCategory) : TableQuery[LowestOfferListings] = {
    apply(tableName(date, pc))
  }

  def tableName(date:Date, pc: ProductCategory) = {
    val suffix = yearMonthSuffix(date)
    s"LOWEST_OFFER_LISTINGS_${pc.baseName}_$suffix"
  }

  def yearMonthSuffix(date:Date) = {
    val Array(yyyy,mm,dd) = date.toString.split('-')
    s"$yyyy$mm"
  }

  def createIfNotExists(pc: ProductCategory, date:Date, db:Database)(implicit ec: ExecutionContext): Future[Vector[MTable]] = {
    db.run(MTable.getTables).andThen{case s =>
      s.map{tables =>
        val name = tableName(date, pc)
        val tableQry: TableQuery[LowestOfferListings] = apply(name)
        if(!tables.toList.contains(name)){
          db.run(tableQry.schema.create)
        }
      }
    }
  }

}
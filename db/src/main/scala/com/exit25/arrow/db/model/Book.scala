package com.exit25.arrow.db.model

import java.sql.Date
import slick.driver.PostgresDriver.api._


/**
 * Created by masinoa on 10/17/15.
 */

case class Book( asin:String,
                    browseNodeID:String,
                    browseNodeName:String,
                    detailPageURL:String,
                    moreOffersUrl: Option[String],
                    title: String,
                    author:String,
                    edition: Option[Int],
                    isbn : String,
                    binding : Option[String],
                    publisher: Option[String],
                    publicationYear: Option[Int],
                    height : Int,
                    length : Int,
                    width : Int,
                    lengthUnit : String,
                    weight : Int,
                    weightUnit : String,
                    createdOn : Date)

class Books(tag: Tag) extends Table[Book](tag, BooksCategory.baseName) {
  def asin: Rep[String] = column[String]("ASIN", O.PrimaryKey)
  def browseNodeId = column[String]("BROWSE_NODE_ID")
  def browseNodeName = column[String]("BROWSE_NODE_NAME")
  def detailPageURL : Rep[String] = column[String]("PURCHASE_URL")
  def moreOffersURL = column[Option[String]]("MORE_OFFERS_URL")
  def title: Rep[String] = column[String]("TITLE")
  def author: Rep[String] = column[String]("AUTHOR")
  def edition: Rep[Option[Int]] = column[Option[Int]]("EDITION")
  def isbn: Rep[String] = column[String]("ISBN")
  def binding = column[Option[String]]("BINDING")
  def publisher = column[Option[String]]("PUBLISHER")
  def publicationYear = column[Option[Int]]("PUBLISHED_ON")
  def height = column[Int]("HEIGHT")
  def length = column[Int]("LENGTH")
  def width = column[Int]("WIDTH")
  def lengthUnit = column[String]("LENGTH_UNIT")
  def weight = column[Int]("WEIGHT")
  def weightUnit = column[String]("WEIGHT_UNIT")
  def createdOn = column[Date]("CREATED_ON")

  def * =(asin, browseNodeId, browseNodeName, detailPageURL,moreOffersURL,
           title, author, edition, isbn, binding, publisher,
    publicationYear,height, length, width, lengthUnit,
    weight, weightUnit, createdOn) <> (Book.tupled,Book.unapply)


}

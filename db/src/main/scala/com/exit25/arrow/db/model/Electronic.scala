package com.exit25.arrow.db.model

import java.sql.Date

import java.sql.Date
import slick.driver.PostgresDriver.api._

/**
 * Created by masinoa on 1/30/16.
 */
case class Electronic(asin:String,
                      upc:String,
                      brand:String,
                      manufacturer:String,
                      title: String,
                      binding : Option[String],
                      detailPageURL:String,
                      moreOffersUrl: Option[String],
                      browseNodeID:String,
                      browseNodeName:String,
                      weight : Int,
                      weightUnit : String,
                      length : Int,
                      height : Int,
                      width : Int,
                      lengthUnit : String,
                      createdOn : Date) {
}

class Electronics(tag:Tag) extends Table[Electronic](tag, ElectronicsCategory.baseName){
  def asin: Rep[String] = column[String]("ASIN", O.PrimaryKey)
  def upc = column[String]("UPC")
  def brand = column[String]("BRAND")
  def manufacturer = column[String]("MANUFACTURER")
  def title: Rep[String] = column[String]("TITLE")
  def binding = column[Option[String]]("BINDING")
  def detailPageURL : Rep[String] = column[String]("PURCHASE_URL")
  def moreOffersURL = column[Option[String]]("MORE_OFFERS_URL")
  def browseNodeId = column[String]("BROWSE_NODE_ID")
  def browseNodeName = column[String]("BROWSE_NODE_NAME")
  def weight = column[Int]("WEIGHT")
  def weightUnit = column[String]("WEIGHT_UNIT")
  def length = column[Int]("LENGTH")
  def height = column[Int]("HEIGHT")
  def width = column[Int]("WIDTH")
  def lengthUnit = column[String]("LENGTH_UNIT")
  def createdOn = column[Date]("CREATED_ON")

  def * = (asin, upc, brand, manufacturer, title,
           binding, detailPageURL, moreOffersURL,
           browseNodeId, browseNodeName, weight, weightUnit,
           length, height, width, lengthUnit, createdOn) <> (Electronic.tupled, Electronic.unapply)
}

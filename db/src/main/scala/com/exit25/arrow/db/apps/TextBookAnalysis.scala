package com.exit25.arrow.db.apps

import java.io.{BufferedWriter, File, FileWriter, PrintWriter}
import java.sql.Date

import com.exit25.arrow.db.model._
import com.typesafe.config.ConfigFactory
import slick.driver.PostgresDriver
import slick.driver.PostgresDriver.api._

import scala.annotation.tailrec
import scala.compat.Platform
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/**
 * Created by masinoa on 1/7/16.
 */
object TextBookAnalysis extends AnalysisFileHelper{

  val now = Platform.currentTime
  val yesterday = new Date(now - 24*60*60*1000)
  //val yesterday = new Date(116, 2, 3)

  def queryLOLs(batch:List[String], remain:List[String],
                db:PostgresDriver.backend.DatabaseDef,
                 f1:String,
                 tif:String,
                 batchSize : Int = 100,
                 combinedFutures: Option[Future[Any]] = None): Future[Any] = {

    val booksTable = TableQuery[Books]

    // get the table corresponding to yesterdays year and month
    // STILL NEED TO FILTER DOWN TO THE DAY
    val lolTable = LowestOfferListingsTableQuery(yesterday, BooksCategory)
    val cpTable = CompetitivePricesTableQuery(yesterday, BooksCategory)
    val srTable = SalesRanksTableQuery(yesterday, BooksCategory)


    val yesterdayLol = lolTable.filter(_.createdOn === yesterday)
    val yesterdayCP = cpTable.filter(_.createdOn === yesterday)
    val yesterdaySR = srTable.filter(_.createdOn === yesterday)


    val q1 = booksTable.filter{v => v.asin inSet batch}

    val innerJoin = for {
      (((b,cp),lol),sr) <- q1 join yesterdayCP on (_.asin === _.asin) join yesterdayLol on (_._1.asin === _.asin) join yesterdaySR on (_._1._1.asin === _.asin)
    } yield (b.asin, b.title, b.detailPageURL, b.moreOffersURL,
        b.publicationYear, sr.salesRank,
        cp.buyBoxNewLandedPrice,cp.buyBoxUsedLandedPrice, cp.tradeInValue,
        lol.fulfillment, lol.subCondition, lol.sellerFeedback, lol.landedPrice)

    val subConds = List("Mint", "New", "VeryGood")
    val feedbacks = List("90-94%", "95-97%", "98-100%")

    val q2 = innerJoin.filter(v => v._11 inSet subConds).filter(v => v._12 inSet feedbacks)

    val cf: Future[Any] = combinedFutures match{
      case Some(fc) => fc.andThen{case x =>
        val xf = db.run(q2.result).andThen{case s =>
          s.map{rsltSeq =>
            val rslt = rsltSeq.toList
            //println(s"query LOLS for ${batch.head}, batch.length=${batch.length}, remain.length=${remain.length}, remain.isEmpty=${remain.isEmpty}")
            processLols(f1,tif,rslt)
          }
        }
          Await.result(xf,Duration.Inf)
          xf
      }
      case _ => {
        val xf = db.run(q2.result).andThen{case s =>
        s.map{rsltSeq =>
          val rslt = rsltSeq.toList
          //println(s"query LOLS for ${batch.head}, batch.length=${batch.length}, remain.length=${remain.length}, remain.isEmpty=${remain.isEmpty}")
          processLols(f1,tif,rslt)
        }
      }
        Await.result(xf,Duration.Inf)
        xf
      }
    }

    if(remain.isEmpty){
      cf
    }else{
      queryLOLs(remain.take(batchSize), remain.drop(batchSize), db, f1, tif, batchSize, Some(cf))
    }

  }

  def processLols(f1:String, tif:String, s: List[(String, String, String, Option[String], Option[Int],
                                                    Int, Option[BigDecimal], Option[BigDecimal], Option[BigDecimal],
                                                    String, Option[String], Option[String], BigDecimal)]) : Unit = {

    val bookLolMap = (Map.empty[String, (B, List[L])] /: s) { (m, x) =>
      val b = B(x._1, x._2, x._3, x._4, x._5, x._6, x._7, x._8, x._9)
      val l = L(x._10, x._11, x._12, x._13)
      val lols = m.getOrElse(b.asin, (b, List.empty[L]))._2
      m.+(b.asin ->(b, l :: lols))
    }

    val transactionCost = 5

    val tradeInWinners = (List.empty[TradeIn] /: bookLolMap.values) { (l,t) =>
      val b = t._1
      val lols = t._2
      val s: Option[L] = None

      //find lowest offer
      if(lols.isEmpty) l
      else {
        val s: Option[L] = None
        //find the lowest offer
        val lo = (s /: lols){(lo,no) =>
          lo match {
            case Some(o) => if(o.landedPrice < no.landedPrice) lo else Some(no)
            case _ => Some(no)
          }
        }
        lo match{
          case Some(o) => {
            val tiv = b.tradeInValue.getOrElse(o.landedPrice - 1)
            if(tiv > o.landedPrice + transactionCost) TradeIn(b, o)::l else l
          }
          case _ => l
        }
      }
    }

    appendToFile(tif){pw =>
      tradeInWinners.foreach{ tiw =>
        val title = tiw.book.title.replace(",", " ")
        val asin = tiw.book.asin
        val pubYear = tiw.book.publicationYear.getOrElse(-1)
        val rank = tiw.book.salesRank
        val tiv = tiw.book.tradeInValue.getOrElse(0)
        val lop = tiw.lowestOffer.landedPrice
        val loCond = tiw.lowestOffer.subCondition.getOrElse("UNK")
        val loFeedBack = tiw.lowestOffer.sellerFeedback.getOrElse("UNK")
        val url = tiw.book.moreOffersURL.getOrElse("")
        pw.println(s"$asin, $title, $pubYear, $rank, $tiv, $lop, $loCond, $loFeedBack, $url")
      }
    }

    val candidates = (List.empty[Buyable] /: bookLolMap.values) { (l, t) =>
      val b = t._1
      val lols = t._2
      val s: Option[L] = None

      //find merchant low
      val ml: Option[L] = (s /: lols) { (lo, no) =>
        if (no.fulfillment.trim.toLowerCase == "merchant") {
          lo match {
            case Some(o) => if (o.landedPrice < no.landedPrice) lo else Some(no)
            case _ => Some(no)
          }
        } else lo
      }

      //get all fba offers
      val fbaOffers : List[L] = (List.empty[L]/:lols){(l, no) =>
        if(no.fulfillment.trim.toLowerCase == "amazon") no::l else l
      }

      //find lowest fba offer, need this to do the transaction cost filtering
      val fl: Option[L] = (s /: lols) { (lo, no) =>
        if (no.fulfillment.trim.toLowerCase == "amazon") {
          lo match {
            case Some(o) => if (o.landedPrice < no.landedPrice) lo else Some(no)
            case _ => Some(no)
          }
        } else lo
      }

      ml match {
        case Some(mlow) => {
          fl match {
            case Some(flow) => {
              val flowlp = flow.landedPrice
              val bbn = b.buyBoxNewLandedPrice.getOrElse(flowlp+1)
              val diff = if(flowlp<bbn)flowlp - mlow.landedPrice - transactionCost
              else bbn - mlow.landedPrice - transactionCost
              if (diff > 0) Buyable(b, fbaOffers, mlow) :: l else l
            }
            case _ => {
              val bbn = b.buyBoxNewLandedPrice.getOrElse(BigDecimal(0))
              val diff = bbn - mlow.landedPrice - transactionCost
              if (diff > 0) Buyable(b, fbaOffers, mlow) :: l else l
            }
          }

        }
        case _ => l //there is NO acceptable merchant offer for this book
      }

    } // end candidates
    println(s"trying to append ${candidates.length} candidates")
    appendToFile(f1) { pw =>
      candidates.foreach { c =>
          val roiLines = c.ROIs.map { roi =>
            val r = f"${roi.roi}%1.3f"
            val p = f"${roi.avgFbaLanded}%1.2f"
            s"${roi.bin+1},$r,$p"
          }

          val title = c.book.title.replace(",", " ")
          val asin = c.book.asin
          val pubYear = c.book.publicationYear.getOrElse(-1)
          val rank = c.book.salesRank
          val ml = c.mercLow.landedPrice
          val mlCond = c.mercLow.subCondition.getOrElse("UNK")
          val mlFeedBack = c.mercLow.sellerFeedback.getOrElse("UNK")
          val url = c.book.moreOffersURL.getOrElse("")
          val rlength = roiLines.length
          roiLines.foreach { rl =>
            val l = s"$asin, $rl, $title, $pubYear, $rank, $ml, $mlCond, $mlFeedBack, $url"
            pw.println(l)
          }
        }
    }

  }


  def run() : Unit = {
    val config = ConfigFactory.parseFile(new File("./conf/application.conf"))
    val db: PostgresDriver.backend.DatabaseDef = Database.forConfig("arrow.db", config)
    val analysisConfig = config.getConfig("arrow.analysis")
    val f1 = analysisConfig.getString("f1")
    val tif = analysisConfig.getString("tradeInFile")
    val sendNotifications = analysisConfig.getBoolean("sendNotifications")
    val batchSize = analysisConfig.getInt("batchsize")

    val header = "ASIN, ROI_BIN, ROI, ROI_AVG_FBA_LANDED_PRICE, TITLE, PUB_YEAR, SALES_RANK, MERC_LOW, MERC_LOW_COND, ML_FEED_BACK, ALL_OFFERS_URL"
    printToFile(f1){pw => pw.println(header)}

    val header2 = "ASIN, TITLE, PUB_YEAR, SALES_RANK, TRADE_IN_VALUE, LO_LANDED_PRICE, LO_COND, LO_FEED_BACK, ALL_OFFERS_URL"
    printToFile(tif){pw => pw.println(header2)}

    try{

      val booksTable = TableQuery[Books]

      // get the table corresponding to yesterdays year and month
      // STILL NEED TO FILTER DOWN TO THE DAY
      val lolTable = LowestOfferListingsTableQuery(yesterday, BooksCategory)
      val cpTable = CompetitivePricesTableQuery(yesterday, BooksCategory)
      val srTable = SalesRanksTableQuery(yesterday, BooksCategory)

      val yesterdaySR = srTable.filter(_.createdOn === yesterday)

      // get asin's for books with a sales rank from yesterday
      val innerJoin = for {
        (b,sr) <- booksTable join yesterdaySR on (_.asin === _.asin)
      } yield (b.asin, b.publicationYear, sr.salesRank)

      val criteriaSalesRankMax = Option(1000000)
      val criteriaPubYear = Option(2009)
      val criteriaSalesRankMin = Option(0) //eliminates unknown sales ranks denoted by -1

      val q0 = for (i <- innerJoin.filter{item =>
        List(
        criteriaSalesRankMax.map(item._3 < _),
        criteriaSalesRankMin.map(item._3 > _),
        criteriaPubYear.map(item._2.getOrElse(0) >= _)
        ).collect({case Some(criteria)  => criteria}).reduceLeftOption( _ && _).getOrElse(false: Rep[Boolean])
      }) yield i._1

      val f0 = db.run(q0.result).andThen{case s =>
        s.map{asinSeq =>
          val asins = asinSeq.toList
          println(s"Result q0, ASINS=${asins.length}")
          val qlols = queryLOLs(asins.take(batchSize), asins.drop(batchSize), db, f1, tif, batchSize, None).andThen{case x =>
            if(sendNotifications){
              Email.sendEmail(s"TextBookAnalysis_$yesterday", "Textbook analysis files attached.", Some(List(f1,tif)))
            }
            println(s"Done Analysis for $yesterday")
          }
          Await.result(qlols, Duration.Inf)
        }
      }

      Await.result(f0, Duration.Inf)

    }catch{
      case e:Exception=>println(e.getStackTrace)
    }

  }//end main
}

case class Buyable(book:B, fbaOffers: List[L], mercLow: L) {

  @tailrec
  private def rois(sum:BigDecimal, count: Int, offers:List[L], currentBasePrice:Option[BigDecimal]=None,
                   R:List[ROI] = List.empty[ROI],
                    bin:Int = 0) : List[ROI] = {
    currentBasePrice match {
      case Some(cbp) =>{
        if(offers.isEmpty){
          val r = ROI(sum/count, bin, mercLow)
          r::R
        }else{
          val np = offers.head
          if(np.landedPrice<cbp+5){
            rois(sum+np.landedPrice, count+1, offers.tail, currentBasePrice, R, bin)
          }
          else{
            val r = ROI(sum/count, bin, mercLow)
            rois(np.landedPrice, 1, offers.tail, Some(np.landedPrice), r::R, bin+1)
          }
        }
      }
      case _ => { //first time through
        if(offers.isEmpty){
          //there are no fba offers
          val fa = book.buyBoxNewLandedPrice match{
            case Some(p) => p
            case _ => book.buyBoxUsedLandedPrice.getOrElse(mercLow.landedPrice)
          }
          List(ROI(fa, 0, mercLow))
        }else{
          //need to kick off the recursion using lowest of buyBoxNew and fba low
          val np = offers.head
          book.buyBoxNewLandedPrice match{
            case Some(bbn) =>{
              if(bbn<np.landedPrice)rois(bbn, 1,offers, Some(bbn))
              else rois(np.landedPrice, 1, offers.tail, Some(np.landedPrice))
            }
            case _ => rois(np.landedPrice, 1, offers.tail, Some(np.landedPrice))
          }

        }
      }
    }
  }

  val ROIs = {
    val sortedFbaOffers = fbaOffers.sortWith(_.landedPrice < _.landedPrice)
    rois(0, 0, sortedFbaOffers).reverse
  }

}

case class TradeIn(book : B, lowestOffer:L)


//simplified representation of Book class for use with the queries in this analysis
case class B(asin:String,
             title:String,
             detailPageURL:String,
             moreOffersURL:Option[String],
             publicationYear: Option[Int],
             salesRank: Int,
             buyBoxNewLandedPrice:Option[BigDecimal],
             buyBoxUsedLandedPrice:Option[BigDecimal],
              tradeInValue:Option[BigDecimal])

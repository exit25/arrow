package com.exit25.arrow.db.model

import java.sql.Date
import java.util.Calendar

import slick.driver.PostgresDriver.api._
import slick.jdbc.meta.MTable
import slick.lifted.TableQuery

import scala.concurrent.{ExecutionContext, Future}

/**
 * Created by masinoa on 12/2/15.
 */
case class CompetitivePrice ( id:Option[Int],
                              asin: String,
                         tradeInValue: Option[BigDecimal],
                         buyBoxNewListPrice: Option[BigDecimal],
                         buyBoxNewLandedPrice: Option[BigDecimal],
                         buyBoxUsedListPrice: Option[BigDecimal],
                         buyBoxUsedLandedPrice: Option[BigDecimal],
                         createdOn : Date
)

class CompetitivePrices(tag: Tag, tableName: String = "COMPETITIVE_PRICES")
  extends Table[CompetitivePrice](tag, tableName){

  def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
  def asin = column[String]("ASIN")
  def tradeInValue = column[Option[BigDecimal]]("TRADE_IN_VALUE")
  def buyBoxNewListPrice = column[Option[BigDecimal]]("BUY_BOX_NEW_LIST_PRICE")
  def buyBoxNewLandedPrice = column[Option[BigDecimal]]("BUY_BOX_NEW_LANDED_PRICE")
  def buyBoxUsedListPrice = column[Option[BigDecimal]]("BUY_BOX_USED_LIST_PRICE")
  def buyBoxUsedLandedPrice = column[Option[BigDecimal]]("BUY_BOX_USED_LANDED_PRICE")
  def createdOn = column[Date]("CREATED_ON")
  def idx = index("IDX_ASIN", (asin))

  def * = (id.?, asin, tradeInValue, buyBoxNewListPrice, buyBoxNewLandedPrice,
    buyBoxUsedListPrice, buyBoxUsedLandedPrice, createdOn) <>(CompetitivePrice.tupled,CompetitivePrice.unapply)
}

object CompetitivePricesTableQuery{

  def apply(tableName:String) : TableQuery[CompetitivePrices] = {
    TableQuery[CompetitivePrices]((tag:Tag) => new CompetitivePrices(tag, tableName))
  }

  def apply(date: Date, pc:ProductCategory) : TableQuery[CompetitivePrices] = {
    apply(tableName(date, pc))
  }

  def tableName(date:Date, pc:ProductCategory) = {
    val dateSuffix = yearMonthSuffix(date)
    s"COMPETITIVE_PRICES_${pc.baseName}_$dateSuffix"
  }

  def yearMonthSuffix(date:Date) = {
    val Array(yyyy,mm,dd) = date.toString.split('-')
    s"$yyyy$mm"
  }

  def createIfNotExists(pc:ProductCategory, date:Date, db:Database)(implicit ec: ExecutionContext): Future[Vector[MTable]] = {
    db.run(MTable.getTables).andThen{case s =>
      s.map{tables =>
        val name = tableName(date, pc)
        val tableQry: TableQuery[CompetitivePrices] = apply(name)
        if(!tables.toList.contains(name)){
          db.run(tableQry.schema.create)
        }
      }
    }
  }

}

package com.exit25.arrow.db.apps

/**
  * Created by masinoa on 9/10/16.
  */
object Main {

  def main(args : Array[String]) : Unit = {
    val appNum = if (args.length < 1) {
      readApplicationNumberFromUser()
    } else {
      processSelection(args(0)) match {
        case Right(i) => i
        case Left(s) => {
          println(s)
          readApplicationNumberFromUser()
        }
      }
    }

    if (appNum == 0) System.exit(0)
    else if (appNum == 1) {
      println("Running DB Builder Application ...")
      DBBuilder.run()
    } else if (appNum == 2) {
      println("Running Text Book ROI Analysis")
      TextBookAnalysis.run()
    } else if (appNum == 3) {
      println("Running Electronics ROI Analysis")
      ElectronicsAnalysis.run()
    }
  }

  def processSelection(selection: String) : Either[String, Int] = {
    try{
      val i = selection.trim.toInt
      if(i > -1 && i<4)Right(i)
      else Left("Selection must be an integer in [0,5]")
    }catch{
      case nfe: NumberFormatException => Left("Selection must be an integer")
      case e : Exception => Left("Please enter a valid selection")
    }
  }

  def readApplicationNumberFromUser() : Int = {

    println("Please select the application you wish to run:")
    println("[0] Quit Application")
    println("[1] DB Builder")
    println("[2] Text Books ROI Analysis")
    println("[3] Electronics ROI Analysis")
    processSelection(scala.io.StdIn.readLine()) match{
      case Right(i) => i
      case Left(s) => {
        println(s)
        readApplicationNumberFromUser()
      }
    }
  }

  def shutdown(){
    println("Shutdown called")
    System.exit(0)
  }

}



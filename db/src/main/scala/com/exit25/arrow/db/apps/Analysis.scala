package com.exit25.arrow.db.apps

import java.io.{BufferedWriter, File, FileWriter, PrintWriter}

/**
  * Created by masinoa on 9/10/16.
  */
trait AnalysisFileHelper {
  def printToFile(path: String)(op: PrintWriter => Unit) {
    val f = new File(path)
    if (f.exists) f.delete
    if (!f.getParentFile.exists())f.getParentFile.mkdirs()
    val pw = new PrintWriter(new File(path))
    try {
      op(pw)
    } finally {
      pw.close()
    }
  }

  def appendToFile(path: String)(op: PrintWriter => Unit) {
    val f = new File(path)
    if (!f.exists()){
      if(!f.getParentFile.exists())f.getParentFile.mkdirs()
      f.createNewFile()
    }
    val pw = new PrintWriter(new BufferedWriter(new FileWriter(path, true)))
    try { {
      op(pw)
    }
    } finally {
      pw.close()
    }
  }

  def getListOfFiles(dir: String):List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList
    } else {
      List[File]()
    }
  }
}

case class ROI(avgFbaLanded:BigDecimal, bin:Int, mercLow: L) {

  val roi: BigDecimal = {
    val ml = mercLow.landedPrice
    val fl = avgFbaLanded
    if(ml<fl){
      fl/ml - 1.0
    }else{
      ml/fl - 1.0
    }
  }

}

//simplified representation of LowestOfferListing class for use with the queries in this anlaysis
case class L(fulfillment: String,
             subCondition: Option[String],
             sellerFeedback: Option[String],
             landedPrice: BigDecimal
            )


package com.exit25.arrow.db

import com.typesafe.config.Config

/**
 * Created by masinoa on 12/5/15.
 */
object ConnectionFactory {

  def connect2pgsql(path:String, config:Config) = {
   slick.driver.PostgresDriver.api.Database.forConfig(path, config)
  }

}
